﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Principal))
        Me.btn_SalirAplicacion = New Proyecto_2__LFP_1S2016_.BonfireButton()
        Me.AirFoxHeader22 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxHeader2()
        Me.AirFoxHeader21 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxHeader2()
        Me.btn_Cuentos = New Proyecto_2__LFP_1S2016_.BonfireButton()
        Me.AirFoxHeader11 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxHeader1()
        Me.btn_Desarrollador = New Proyecto_2__LFP_1S2016_.BonfireButton()
        Me.SuspendLayout()
        '
        'btn_SalirAplicacion
        '
        Me.btn_SalirAplicacion.ButtonStyle = Proyecto_2__LFP_1S2016_.BonfireButton.Style.Dark
        Me.btn_SalirAplicacion.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_SalirAplicacion.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btn_SalirAplicacion.Image = Nothing
        Me.btn_SalirAplicacion.Location = New System.Drawing.Point(275, 255)
        Me.btn_SalirAplicacion.Name = "btn_SalirAplicacion"
        Me.btn_SalirAplicacion.RoundedCorners = False
        Me.btn_SalirAplicacion.Size = New System.Drawing.Size(104, 26)
        Me.btn_SalirAplicacion.TabIndex = 5
        Me.btn_SalirAplicacion.Text = "Salir"
        '
        'AirFoxHeader22
        '
        Me.AirFoxHeader22.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.AirFoxHeader22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.AirFoxHeader22.Location = New System.Drawing.Point(459, 255)
        Me.AirFoxHeader22.Name = "AirFoxHeader22"
        Me.AirFoxHeader22.Size = New System.Drawing.Size(103, 19)
        Me.AirFoxHeader22.TabIndex = 4
        Me.AirFoxHeader22.Text = "Maestro/Padre"
        '
        'AirFoxHeader21
        '
        Me.AirFoxHeader21.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.AirFoxHeader21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.AirFoxHeader21.Location = New System.Drawing.Point(100, 255)
        Me.AirFoxHeader21.Name = "AirFoxHeader21"
        Me.AirFoxHeader21.Size = New System.Drawing.Size(96, 19)
        Me.AirFoxHeader21.TabIndex = 3
        Me.AirFoxHeader21.Text = "Desarrollador"
        '
        'btn_Cuentos
        '
        Me.btn_Cuentos.ButtonStyle = Proyecto_2__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btn_Cuentos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_Cuentos.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btn_Cuentos.Image = CType(resources.GetObject("btn_Cuentos.Image"), System.Drawing.Image)
        Me.btn_Cuentos.Location = New System.Drawing.Point(416, 104)
        Me.btn_Cuentos.Name = "btn_Cuentos"
        Me.btn_Cuentos.RoundedCorners = True
        Me.btn_Cuentos.Size = New System.Drawing.Size(186, 145)
        Me.btn_Cuentos.TabIndex = 2
        '
        'AirFoxHeader11
        '
        Me.AirFoxHeader11.Font = New System.Drawing.Font("Segoe UI Semibold", 20.0!)
        Me.AirFoxHeader11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.AirFoxHeader11.Location = New System.Drawing.Point(55, 24)
        Me.AirFoxHeader11.Name = "AirFoxHeader11"
        Me.AirFoxHeader11.Size = New System.Drawing.Size(547, 51)
        Me.AirFoxHeader11.TabIndex = 1
        Me.AirFoxHeader11.Text = "Bienvenido a la Biblioteca de Audio Cuentos"
        '
        'btn_Desarrollador
        '
        Me.btn_Desarrollador.ButtonStyle = Proyecto_2__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btn_Desarrollador.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_Desarrollador.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btn_Desarrollador.Image = CType(resources.GetObject("btn_Desarrollador.Image"), System.Drawing.Image)
        Me.btn_Desarrollador.Location = New System.Drawing.Point(55, 104)
        Me.btn_Desarrollador.Name = "btn_Desarrollador"
        Me.btn_Desarrollador.RoundedCorners = True
        Me.btn_Desarrollador.Size = New System.Drawing.Size(186, 145)
        Me.btn_Desarrollador.TabIndex = 0
        '
        'frm_Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(658, 330)
        Me.Controls.Add(Me.btn_SalirAplicacion)
        Me.Controls.Add(Me.AirFoxHeader22)
        Me.Controls.Add(Me.AirFoxHeader21)
        Me.Controls.Add(Me.btn_Cuentos)
        Me.Controls.Add(Me.AirFoxHeader11)
        Me.Controls.Add(Me.btn_Desarrollador)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_Principal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Proyecto #2 - Mike Molina 201212535"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btn_Desarrollador As BonfireButton
    Friend WithEvents AirFoxHeader11 As Theme.AirFoxHeader1
    Friend WithEvents btn_Cuentos As BonfireButton
    Friend WithEvents AirFoxHeader21 As Theme.AirFoxHeader2
    Friend WithEvents AirFoxHeader22 As Theme.AirFoxHeader2
    Friend WithEvents btn_SalirAplicacion As BonfireButton
End Class
