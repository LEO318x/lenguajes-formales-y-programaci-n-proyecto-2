﻿Imports System.Speech.Synthesis

Public Class frm_Cuentos
    Dim leerTexto = False, resaltarTexto = False, mostrarTexto = False
    Dim volumen = 0, velocidad = 0, voz = 0
    Dim textocuento = ""
    Dim hablar As New SpeechSynthesizer

    Private Sub frm_Desarrollador_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cerrarFormulario(e)
    End Sub

    Private Sub Speaker_SpeakProgress(ByVal sender As Object, ByVal e As System.Speech.Synthesis.SpeakProgressEventArgs)
        rtb_Cuento.HideSelection = False
        Dim iPos As Integer = e.CharacterPosition
        Dim iLen As Integer = e.CharacterCount
        rtb_Cuento.SelectionStart = iPos
        rtb_Cuento.SelectionLength = iLen
        rtb_Cuento.SelectionColor = Color.Green
    End Sub


    Private Sub btn_Play_Click(sender As Object, e As EventArgs) Handles btn_Play.Click
        hablar.SpeakAsyncCancelAll()
        If (leerTexto And resaltarTexto And mostrarTexto) Then
            hablar.Volume = volumen
            hablar.Rate = velocidad
            If (voz = 1) Then
                Console.WriteLine("Mujer")
                hablar.SelectVoice("IVONA 2 Conchita")
            ElseIf (voz = 2) Then
                Console.WriteLine("Hombre")
                hablar.SelectVoice("IVONA 2 Enrique")
            End If
            AddHandler hablar.SpeakProgress, AddressOf Speaker_SpeakProgress
            hablar.SpeakAsync(rtb_Cuento.Text)
        ElseIf (leerTexto And mostrarTexto) Then
            hablar.Volume = volumen
            hablar.Rate = velocidad
            If (voz = 1) Then
                hablar.SelectVoice("IVONA 2 Conchita")
            ElseIf (voz = 2) Then
                hablar.SelectVoice("IVONA 2 Enrique")
            End If
            hablar.SpeakAsync(rtb_Cuento.Text)
        ElseIf (leerTexto) Then
            hablar.Volume = volumen
            hablar.Rate = velocidad
            If (voz = 1) Then
                hablar.SelectVoice("IVONA 2 Conchita")
            ElseIf (voz = 2) Then
                hablar.SelectVoice("IVONA 2 Enrique")
            End If
            hablar.SpeakAsync(textocuento)
        End If

    End Sub

    Private Sub btn_Stop_Click(sender As Object, e As EventArgs) Handles btn_Stop.Click
        hablar.SpeakAsyncCancelAll()
    End Sub

    Public Sub pararReproduccion()
        hablar.SpeakAsyncCancelAll()
    End Sub

    Private Sub cerrarFormulario(e As Object)
        If e.CloseReason = System.Windows.Forms.CloseReason.UserClosing Then
            'Cerrado desde la x del formulario o Alt + F4
            Dim salirOpcion As Integer = MessageBox.Show("¿Estas seguro que deseas regresar al menu principal?", "Salir", MessageBoxButtons.YesNo)

            If (salirOpcion = DialogResult.Yes) Then
                Me.Hide()
                'Regresa al menu principal

                'Dim frmmenu As New frm_Principal()
                'frmmenu.Show()
                hablar.SpeakAsyncCancelAll()
                frm_Principal.Show()

                ''Cierra el formulario
                'e.Cancel = False

                'No hace nada
                e.Cancel = True

            Else
                'No hace nada
                e.Cancel = True
            End If
        Else
            'Cerrado por otra razón      

        End If
    End Sub


    Public Sub agregarTextoCuento(Texto As String)
        rtb_Cuento.Text = Texto
    End Sub

    Public Sub met_leerTexto(valor As Integer)
        If (valor = 1) Then
            leerTexto = True
            AirFoxAlertBox5.Visible = True
            AirFoxAlertBox6.Visible = False
        Else
            leerTexto = False
            AirFoxAlertBox6.Visible = True
            AirFoxAlertBox5.Visible = False
        End If
    End Sub

    Public Sub met_mostrarTexto(valor As Integer)
        If (valor = 1) Then
            mostrarTexto = True
            AirFoxAlertBox1.Visible = True
            AirFoxAlertBox2.Visible = False
        Else
            mostrarTexto = False
            AirFoxAlertBox2.Visible = True
            AirFoxAlertBox1.Visible = False
            rtb_Cuento.Text = ""
        End If
    End Sub

    Public Sub met_resaltarTexto(valor As Integer)
        If (valor = 1) Then
            resaltarTexto = True
            AirFoxAlertBox3.Visible = True
            AirFoxAlertBox4.Visible = False
        Else
            resaltarTexto = False
            AirFoxAlertBox4.Visible = True
            AirFoxAlertBox3.Visible = False

        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)

    End Sub

    Public Sub met_textoCuento(Texto As String)
        textocuento = Texto
    End Sub

    Public Sub modificarVolumen(valor As Integer)
        volumen = valor
    End Sub

    Public Sub modificarVelocidad(valor As Integer)
        velocidad = valor
    End Sub

    Public Sub modificarTipoVoz(valor As Integer)
        voz = valor
    End Sub

End Class