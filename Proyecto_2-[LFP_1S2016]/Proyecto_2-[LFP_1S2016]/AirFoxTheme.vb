﻿' AirFox Theme.
' Made by AeroRev9.
' Initial Release.

Imports System.ComponentModel
Imports System.Drawing.Drawing2D
Imports System.Drawing.Text

Public Module Helpers

    Public Enum MouseState As Byte
        None = 0
        Over = 1
        Down = 2
    End Enum

    Public Enum RoundingStyle As Byte
        All = 0
        Top = 1
        Bottom = 2
        Left = 3
        Right = 4
        TopRight = 5
        BottomRight = 6
    End Enum

    Public Sub CenterString(G As Graphics, T As String, F As Font, C As Color, R As Rectangle)
        Dim TS As SizeF = G.MeasureString(T, F)

        Using B As New SolidBrush(C)
            G.DrawString(T, F, B, New Point(R.X + R.Width / 2 - (TS.Width / 2), R.Y + R.Height / 2 - (TS.Height / 2)))
        End Using
    End Sub

    Public Function ColorFromHex(Hex As String) As Color
        Return Color.FromArgb(Long.Parse(String.Format("FFFFFFFFFF{0}", Hex.Substring(1)), Globalization.NumberStyles.HexNumber))
    End Function

    Public Function FullRectangle(S As Size, Subtract As Boolean) As Rectangle

        If Subtract Then
            Return New Rectangle(0, 0, S.Width - 1, S.Height - 1)
        Else
            Return New Rectangle(0, 0, S.Width, S.Height)
        End If

    End Function

    Public Function RoundRect(ByVal Rect As Rectangle, ByVal Rounding As Integer, Optional ByVal Style As RoundingStyle = RoundingStyle.All) As GraphicsPath

        Dim GP As New GraphicsPath()
        Dim AW As Integer = Rounding * 2

        GP.StartFigure()

        If Rounding = 0 Then
            GP.AddRectangle(Rect)
            GP.CloseAllFigures()
            Return GP
        End If

        Select Case Style
            Case RoundingStyle.All
                GP.AddArc(New Rectangle(Rect.X, Rect.Y, AW, AW), -180, 90)
                GP.AddArc(New Rectangle(Rect.Width - AW + Rect.X, Rect.Y, AW, AW), -90, 90)
                GP.AddArc(New Rectangle(Rect.Width - AW + Rect.X, Rect.Height - AW + Rect.Y, AW, AW), 0, 90)
                GP.AddArc(New Rectangle(Rect.X, Rect.Height - AW + Rect.Y, AW, AW), 90, 90)
            Case RoundingStyle.Top
                GP.AddArc(New Rectangle(Rect.X, Rect.Y, AW, AW), -180, 90)
                GP.AddArc(New Rectangle(Rect.Width - AW + Rect.X, Rect.Y, AW, AW), -90, 90)
                GP.AddLine(New Point(Rect.X + Rect.Width, Rect.Y + Rect.Height), New Point(Rect.X, Rect.Y + Rect.Height))
            Case RoundingStyle.Bottom
                GP.AddLine(New Point(Rect.X, Rect.Y), New Point(Rect.X + Rect.Width, Rect.Y))
                GP.AddArc(New Rectangle(Rect.Width - AW + Rect.X, Rect.Height - AW + Rect.Y, AW, AW), 0, 90)
                GP.AddArc(New Rectangle(Rect.X, Rect.Height - AW + Rect.Y, AW, AW), 90, 90)
            Case RoundingStyle.Left
                GP.AddArc(New Rectangle(Rect.X, Rect.Y, AW, AW), -180, 90)
                GP.AddLine(New Point(Rect.X + Rect.Width, Rect.Y), New Point(Rect.X + Rect.Width, Rect.Y + Rect.Height))
                GP.AddArc(New Rectangle(Rect.X, Rect.Height - AW + Rect.Y, AW, AW), 90, 90)
            Case RoundingStyle.Right
                GP.AddLine(New Point(Rect.X, Rect.Y + Rect.Height), New Point(Rect.X, Rect.Y))
                GP.AddArc(New Rectangle(Rect.Width - AW + Rect.X, Rect.Y, AW, AW), -90, 90)
                GP.AddArc(New Rectangle(Rect.Width - AW + Rect.X, Rect.Height - AW + Rect.Y, AW, AW), 0, 90)
            Case RoundingStyle.TopRight
                GP.AddLine(New Point(Rect.X, Rect.Y + 1), New Point(Rect.X, Rect.Y))
                GP.AddArc(New Rectangle(Rect.Width - AW + Rect.X, Rect.Y, AW, AW), -90, 90)
                GP.AddLine(New Point(Rect.X + Rect.Width, Rect.Y + Rect.Height - 1), New Point(Rect.X + Rect.Width, Rect.Y + Rect.Height))
                GP.AddLine(New Point(Rect.X + 1, Rect.Y + Rect.Height), New Point(Rect.X, Rect.Y + Rect.Height))
            Case RoundingStyle.BottomRight
                GP.AddLine(New Point(Rect.X, Rect.Y + 1), New Point(Rect.X, Rect.Y))
                GP.AddLine(New Point(Rect.X + Rect.Width - 1, Rect.Y), New Point(Rect.X + Rect.Width, Rect.Y))
                GP.AddArc(New Rectangle(Rect.Width - AW + Rect.X, Rect.Height - AW + Rect.Y, AW, AW), 0, 90)
                GP.AddLine(New Point(Rect.X + 1, Rect.Y + Rect.Height), New Point(Rect.X, Rect.Y + Rect.Height))
        End Select

        GP.CloseAllFigures()

        Return GP

    End Function

End Module

Namespace Base

    Public MustInherit Class LeftTabControl
        Inherits TabControl

        Public BaseRect As Rectangle
        Public OverRect As Rectangle
        Public ItemWidth As Integer = 180

        Private _OverIndex As Integer = -1

        Public ReadOnly Property Hovering As Boolean
            Get
                Return Not OverIndex = -1
            End Get
        End Property

        Public Property OverIndex As Integer
            Get
                Return _OverIndex
            End Get
            Set(value As Integer)
                _OverIndex = value

                If Not _OverIndex = -1 Then
                    OverRect = New Rectangle(GetTabRect(OverIndex).X, GetTabRect(OverIndex).Y, GetTabRect(OverIndex).Width, GetTabRect(OverIndex).Height)
                End If

                Invalidate()
            End Set
        End Property

        Protected Overrides Sub OnCreateControl()
            MyBase.OnCreateControl()
            SetStyle(ControlStyles.UserPaint, True)
        End Sub

        Protected Overrides Sub OnControlAdded(e As ControlEventArgs)
            MyBase.OnControlAdded(e)
            e.Control.BackColor = Color.White
            e.Control.ForeColor = ColorFromHex("#424E5A")
            e.Control.Font = New Font("Segoe UI", 10)
        End Sub

        Sub New()
            DoubleBuffered = True
            Alignment = TabAlignment.Left
            ItemSize = New Size(40, ItemWidth)
            SizeMode = TabSizeMode.Fixed
            Font = New Font("Segoe UI", 10)

        End Sub

        Protected Overrides Sub OnMouseMove(e As MouseEventArgs)

            For I As Integer = 0 To TabPages.Count - 1
                If GetTabRect(I).Contains(e.Location) And Not SelectedIndex = I Then
                    OverIndex = I
                    Exit For
                Else
                    OverIndex = -1
                End If
            Next

            MyBase.OnMouseMove(e)

        End Sub

        Protected Overrides Sub OnMouseLeave(e As EventArgs)
            MyBase.OnMouseLeave(e)
            OverIndex = -1
        End Sub

    End Class

    Public MustInherit Class TopTabControl
        Inherits TabControl

        Public BaseRect As Rectangle
        Public OverRect As Rectangle
        Public ItemWidth As Integer = 180

        Private _OverIndex As Integer = -1

        Public ReadOnly Property Hovering As Boolean
            Get
                Return Not OverIndex = -1
            End Get
        End Property

        Public Property OverIndex As Integer
            Get
                Return _OverIndex
            End Get
            Set(value As Integer)
                _OverIndex = value

                If Not _OverIndex = -1 Then
                    OverRect = New Rectangle(GetTabRect(OverIndex).X, GetTabRect(OverIndex).Y, GetTabRect(OverIndex).Width, GetTabRect(OverIndex).Height)
                End If

                Invalidate()
            End Set
        End Property

        Protected Overrides Sub OnCreateControl()
            MyBase.OnCreateControl()
            SetStyle(ControlStyles.UserPaint, True)
        End Sub

        Protected Overrides Sub OnControlAdded(e As ControlEventArgs)
            MyBase.OnControlAdded(e)
            e.Control.BackColor = Color.White
            e.Control.ForeColor = ColorFromHex("#424E5A")
            e.Control.Font = New Font("Segoe UI", 10)
        End Sub

        Sub New()
            DoubleBuffered = True
            Alignment = TabAlignment.Top
            ItemSize = New Size(ItemWidth, 40)
            SizeMode = TabSizeMode.Fixed
            Font = New Font("Segoe UI", 10)
        End Sub

        Protected Overrides Sub OnMouseMove(e As MouseEventArgs)

            For I As Integer = 0 To TabPages.Count - 1
                If GetTabRect(I).Contains(e.Location) And Not SelectedIndex = I Then
                    OverIndex = I
                    Exit For
                Else
                    OverIndex = -1
                End If
            Next

            MyBase.OnMouseMove(e)

        End Sub

        Protected Overrides Sub OnMouseLeave(e As EventArgs)
            MyBase.OnMouseLeave(e)
            OverIndex = -1
        End Sub

    End Class

    Public MustInherit Class CheckControl
        Inherits Control

        Public Event CheckedChanged(sender As Object, e As EventArgs)
        Public State As MouseState

        Private IsEnabled As Boolean
        Private IsChecked As Boolean

        Public Shadows Property Enabled As Boolean
            Get
                Return EnabledCalc
            End Get
            Set(value As Boolean)
                IsEnabled = value

                If Enabled Then
                    Cursor = Cursors.Hand
                Else
                    Cursor = Cursors.Default
                End If

                Invalidate()
            End Set
        End Property

        <DisplayName("Enabled")>
        Public Property EnabledCalc As Boolean
            Get
                Return IsEnabled
            End Get
            Set(value As Boolean)
                Enabled = value
                Invalidate()
            End Set
        End Property

        Public Property Checked As Boolean
            Get
                Return IsChecked
            End Get
            Set(value As Boolean)
                IsChecked = value
                Invalidate()
            End Set
        End Property

        Sub New()
            Enabled = True
            DoubleBuffered = True
        End Sub

        Protected Overrides Sub OnMouseEnter(e As EventArgs)
            MyBase.OnMouseEnter(e)
            State = MouseState.Over : Invalidate()
        End Sub

        Protected Overrides Sub OnMouseLeave(e As EventArgs)
            MyBase.OnMouseLeave(e)
            State = MouseState.None : Invalidate()
        End Sub

        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)
            State = MouseState.Over : Invalidate()

            If Enabled Then
                Checked = Not Checked
                RaiseEvent CheckedChanged(Me, e)
            End If

        End Sub

        Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
            MyBase.OnMouseDown(e)
            State = MouseState.Down : Invalidate()
        End Sub

    End Class

    Public MustInherit Class BaseRadioButton
        Inherits Control

        Public Event CheckedChanged(sender As Object, e As EventArgs)
        Public State As MouseState

        Private IsEnabled As Boolean
        Private IsChecked As Boolean

        Public Shadows Property Enabled As Boolean
            Get
                Return EnabledCalc
            End Get
            Set(value As Boolean)
                IsEnabled = value

                If Enabled Then
                    Cursor = Cursors.Hand
                Else
                    Cursor = Cursors.Default
                End If

                Invalidate()
            End Set
        End Property

        <DisplayName("Enabled")>
        Public Property EnabledCalc As Boolean
            Get
                Return IsEnabled
            End Get
            Set(value As Boolean)
                Enabled = value
                Invalidate()
            End Set
        End Property

        Public Property Checked As Boolean
            Get
                Return IsChecked
            End Get
            Set(value As Boolean)
                IsChecked = value
                Invalidate()
            End Set
        End Property

        Sub New()
            Enabled = True
            DoubleBuffered = True
        End Sub

        Protected Overrides Sub OnMouseEnter(e As EventArgs)
            MyBase.OnMouseEnter(e)
            State = MouseState.Over : Invalidate()
        End Sub

        Protected Overrides Sub OnMouseLeave(e As EventArgs)
            MyBase.OnMouseLeave(e)
            State = MouseState.None : Invalidate()
        End Sub

        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)
            State = MouseState.Over : Invalidate()

            If Enabled Then

                If Not Checked Then

                    For Each C As Control In Parent.Controls
                        If TypeOf C Is BaseRadioButton Then
                            DirectCast(C, BaseRadioButton).Checked = False
                        End If
                    Next

                End If

                Checked = True
                RaiseEvent CheckedChanged(Me, e)
            End If

        End Sub

        Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
            MyBase.OnMouseDown(e)
            State = MouseState.Down : Invalidate()
        End Sub

    End Class

    Public MustInherit Class ButtonBase
        Inherits Control

        Public Shadows Event Click(sender As Object, e As EventArgs)

        Public State As MouseState
        Private IsEnabled As Boolean

        Public Shadows Property Enabled As Boolean
            Get
                Return EnabledCalc
            End Get
            Set(value As Boolean)
                IsEnabled = value

                If Enabled Then
                    Cursor = Cursors.Hand
                Else
                    Cursor = Cursors.Default
                End If

                Invalidate()
            End Set
        End Property

        <DisplayName("Enabled")>
        Public Property EnabledCalc As Boolean
            Get
                Return IsEnabled
            End Get
            Set(value As Boolean)
                Enabled = value
                Invalidate()
            End Set
        End Property

        Sub New()
            DoubleBuffered = True
            Enabled = True
        End Sub

        Protected Overrides Sub OnMouseEnter(e As EventArgs)
            MyBase.OnMouseEnter(e)
            State = MouseState.Over : Invalidate()
        End Sub

        Protected Overrides Sub OnMouseLeave(e As EventArgs)
            MyBase.OnMouseLeave(e)
            State = MouseState.None : Invalidate()
        End Sub

        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)
            State = MouseState.Over : Invalidate()

            If Enabled Then
                RaiseEvent Click(Me, e)
            End If

        End Sub

        Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
            MyBase.OnMouseDown(e)
            State = MouseState.Down : Invalidate()
        End Sub

    End Class

End Namespace

Namespace Theme

    Public Class AirFoxMainTabControl
        Inherits Base.LeftTabControl

        Private G As Graphics

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            G.Clear(ColorFromHex("#424F5A"))

            For I As Integer = 0 To TabPages.Count - 1

                BaseRect = GetTabRect(I)

                If SelectedIndex = I Then

                    Using SelectionBrush As New SolidBrush(ColorFromHex("#343F48")), TextBrush As New SolidBrush(ColorFromHex("#F2F2F2")), TextFont As New Font("Segoe UI", 10)
                        G.FillRectangle(SelectionBrush, New Rectangle(BaseRect.X - 6, BaseRect.Y + 1, BaseRect.Width + 9, BaseRect.Height - 1))
                        G.DrawString(TabPages(I).Text, TextFont, TextBrush, New Point(BaseRect.X + 55, BaseRect.Y + 11))
                    End Using

                    G.SmoothingMode = SmoothingMode.Default

                    Using LeftBarBrush As New SolidBrush(ColorFromHex("#FF9500"))
                        G.FillRectangle(LeftBarBrush, New Rectangle(BaseRect.X - 2, BaseRect.Y + 2, 4, BaseRect.Height - 2))
                    End Using

                Else

                    Using TextBrush As New SolidBrush(ColorFromHex("#C1C1C1")), TextFont As New Font("Segoe UI", 10)
                        G.DrawString(TabPages(I).Text, TextFont, TextBrush, New Point(BaseRect.X + 55, BaseRect.Y + 11))
                    End Using

                End If

                If Not IsNothing(ImageList) Then
                    If Not TabPages(I).ImageIndex < 0 Then
                        G.DrawImage(ImageList.Images(TabPages(I).ImageIndex), New Rectangle(BaseRect.X + 23, BaseRect.Y + ((BaseRect.Height / 2) - 8), 16, 16))
                    End If
                End If

                If Hovering Then

                    Using HoverBrush As New SolidBrush(ColorFromHex("#5E6972")), TextBrush As New SolidBrush(ColorFromHex("#C1C1C1")), TextFont As New Font("Segoe UI", 10)
                        G.FillRectangle(HoverBrush, New Rectangle(OverRect.X - 6, OverRect.Y + 1, OverRect.Width + 9, OverRect.Height - 1))
                        G.DrawString(TabPages(OverIndex).Text, TextFont, TextBrush, New Point(OverRect.X + 55, OverRect.Y + 11))
                    End Using

                    If Not IsNothing(ImageList) Then
                        If Not TabPages(OverIndex).ImageIndex < 0 Then
                            G.DrawImage(ImageList.Images(TabPages(OverIndex).ImageIndex), New Rectangle(OverRect.X + 23, OverRect.Y + ((OverRect.Height / 2) - 8), 16, 16))
                        End If
                    End If

                End If

            Next

            MyBase.OnPaint(e)

        End Sub

    End Class

    Public Class AirFoxSubTabControl
        Inherits Base.TopTabControl

        Private G As Graphics

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            G.Clear(Parent.BackColor)

            For I As Integer = 0 To TabPages.Count - 1

                BaseRect = GetTabRect(I)

                If SelectedIndex = I Then

                    Using SelectionBrush As New SolidBrush(ColorFromHex("#F0F0F0")), TextBrush As New SolidBrush(ColorFromHex("#424F5A")), TextFont As New Font("Segoe UI", 10)
                        G.FillRectangle(SelectionBrush, New Rectangle(BaseRect.X + 2, BaseRect.Y - 2, BaseRect.Width, BaseRect.Height + 1))
                        CenterString(G, TabPages(I).Text, TextFont, TextBrush.Color, New Rectangle(BaseRect.X + 4, BaseRect.Y, BaseRect.Width, BaseRect.Height))
                    End Using

                    G.SmoothingMode = SmoothingMode.Default

                    Using BottomBarBrush As New SolidBrush(ColorFromHex("#FF9500"))
                        G.FillRectangle(BottomBarBrush, New Rectangle(BaseRect.X + 3, BaseRect.Y + 36, BaseRect.Width - 1, 4))
                    End Using

                Else

                    Using TextBrush As New SolidBrush(ColorFromHex("#424F5A")), TextFont As New Font("Segoe UI", 10)
                        CenterString(G, TabPages(I).Text, TextFont, TextBrush.Color, New Rectangle(BaseRect.X + 4, BaseRect.Y, BaseRect.Width, BaseRect.Height))
                    End Using

                End If

                If Hovering Then

                    Using HoverBrush As New SolidBrush(ColorFromHex("#F0F0F0")), TextBrush As New SolidBrush(ColorFromHex("#C1C1C1")), TextFont As New Font("Segoe UI", 10)
                        G.FillRectangle(HoverBrush, New Rectangle(OverRect.X + 2, OverRect.Y - 2, OverRect.Width, OverRect.Height + 1))
                        CenterString(G, TabPages(OverIndex).Text, TextFont, TextBrush.Color, New Rectangle(OverRect.X + 4, OverRect.Y, OverRect.Width, OverRect.Height))
                    End Using

                End If

            Next

            Using BottomLine As New Pen(ColorFromHex("#C8C8C8"), 1)
                G.DrawLine(BottomLine, 5, 42, Width - 6, 42)
            End Using

            MyBase.OnPaint(e)

        End Sub

    End Class

    <DefaultEvent("CheckedChanged")>
    Public Class AirFoxCheckBox
        Inherits Base.CheckControl

        Private G As Graphics
        Private B64E As String = "iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAMAAABhq6zVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABfVBMVEUAAAArntgsm9srm9sgn98gnuE1m9gsndoqnNkvmdYpnNkqm9gvl9YtnNksntosnNorm9o2mtoumtg/mb8rmtotm9snn9s/kMIqnNosm9ksoNssndksndklndwyl9gqn9krm9ornNksm9ksm9srm9srm9ssnNornNkgnd4xm9ksnNosnNorm9orm9ssnNosnNoalf8rnNkrm9ksnNosm9opnNkpnNkrnNkqm9ksnNosnNktmdcqm9ssnNosnNornNssnNksnNornNornNosm9srnNodnNYsmtosnNornNovmtkqnNornNosm9osnNoqmtorm9opntYim9Msm9osnNorm9osnNorm9oum9oAr9U1mNYqndosnNosnNosnNoxmNEont4qndoqnNosnNosnNosm9krm9kpnNksndornNkrnNksntsulc8pnt0snNosnNosnNosnNosnNosnNosnNosnNosnNosnNosnNosnNosnNosnNosnNornNn///9BLcc1AAAAbnRSTlMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWz8agABKOiVAAOt7jYBAgEABFr/fAABAhjRyg0VZhQDi/tMApWzBUD6kwJtgMTcHgIABKTkuvtkBAAAGtb7vwEBAkPz9zABAAJwdAEAANmSOx8AAAABYktHRH4/uEFzAAAAB3RJTUUH3wscBzYVoy2+dwAAAJxJREFUCNdjYAABWTl5RgVFJTBbmUmFWVUtTx3EZmHV0GTTytfWYdDV02c3MOQwMi4w4WTg4jI1M+e2sCy0suZhsLG147XncyhydOJ3ZnApdnUTcPco8RT0EmLwLi3zEfYt9/MXCQhkCAquCAkNqwwXjYiMYhCLjqmKrY4Tj09gYWFIlEhKrklJlUzjSmdgkMqQzqzNksnOyWVgAABtEB7gG6KeHgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNS0xMS0yOFQwNzo1NDoyMS0wNTowMCRACR8AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTUtMTEtMjhUMDc6NTQ6MjEtMDU6MDBVHbGjAAAAAElFTkSuQmCC"
        Private B64D As String = "iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAMAAABhq6zVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABfVBMVEUAAAC2tbW3s7O2tLS/r6/Brq6vtra1s7O2tLS1srK2s7O2tLSxsLC1s7O3t7e2s7O2tLS1ubm0s7OroqK2tLS3s7O2tbWxqKi2s7O1s7O2t7e2tbW2s7O6sbGxtra4tLS2tLS2tLS1tLS3s7O2tLS2tLS2tLS2s7O9r6+ytbW1s7O2tLS2s7O2tLS2tLS1s7P1ioq1tLS1tLS2tLS2tLS2s7O2s7O2tLS2s7O2tLS1s7OzsbG2tLS2tLS1s7O2tbW2s7O2tLS2s7O2s7O2tLS2tLS6srK2tLS2tLS2s7O1tbW2s7O1s7O2tLS2tLS2s7O2tLSys7OssLC2tLS2tLS1tLS2tLS2s7O2tLTGoKCut7e0tbW2tLS2tLS2tLS1rq63tra3s7O3s7O2tLS2tLS1s7O1s7O2s7O2tbW1tLS1tLS3tLSwsbG3tra2tLS2tLS2tLS2tLS2tLS2tLS2tLS2tLS2tLS2tLS2tLS2tLS2tLS2tLS2tLS1tLT///9wiMU7AAAAbnRSTlMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWz8agABKOiVAAOt7jYBAgEABFr/fAABAhjRyg0VZhQDi/tMApWzBUD6kwJtgMTcHgIABKTkuvtkBAAAGtb7vwEBAkPz9zABAAJwdAEAANmSOx8AAAABYktHRH4/uEFzAAAAB3RJTUUH3wscBzg0cceDpwAAAJxJREFUCNdjYAABWTl5RgVFJTBbmUmFWVUtTx3EZmHV0GTTytfWYdDV02c3MOQwMi4w4WTg4jI1M+e2sCy0suZhsLG147XncyhydOJ3ZnApdnUTcPco8RT0EmLwLi3zEfYt9/MXCQhkCAquCAkNqwwXjYiMYhCLjqmKrY4Tj09gYWFIlEhKrklJlUzjSmdgkMqQzqzNksnOyWVgAABtEB7gG6KeHgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNS0xMS0yOFQwNzo1Njo1Mi0wNTowMBuYyqYAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTUtMTEtMjhUMDc6NTY6NTItMDU6MDBqxXIaAAAAAElFTkSuQmCC"

        Public Property Bold As Boolean

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            G.Clear(Parent.BackColor)

            If Enabled Then

                Select Case State

                    Case MouseState.None

                        Using Border As New Pen(ColorFromHex("#C8C8C8"))
                            G.DrawPath(Border, RoundRect(New Rectangle(0, 0, 20, 20), 2))
                        End Using


                    Case Else

                        Using Border As New Pen(ColorFromHex("#2C9CDA"))
                            G.DrawPath(Border, RoundRect(New Rectangle(0, 0, 20, 20), 2))
                        End Using

                End Select

                Using TextColor As New SolidBrush(ColorFromHex("#424E5A"))

                    If Bold Then

                        Using BFont As New Font("Segoe UI", 10, FontStyle.Bold)
                            G.DrawString(Text, BFont, TextColor, New Point(27, 1))
                        End Using

                    Else
                        Using DFont As New Font("Segoe UI", 10)
                            G.DrawString(Text, DFont, TextColor, New Point(27, 1))
                        End Using

                    End If

                End Using

            Else

                Using Border As New Pen(ColorFromHex("#E6E6E6"))
                    G.DrawPath(Border, RoundRect(New Rectangle(0, 0, 20, 20), 2))
                End Using

                Using TextColor As New SolidBrush(ColorFromHex("#A6B2BE"))

                    If Bold Then

                        Using BFont As New Font("Segoe UI", 10, FontStyle.Bold)
                            G.DrawString(Text, BFont, TextColor, New Point(27, 1))
                        End Using

                    Else
                        Using DFont As New Font("Segoe UI", 10)
                            G.DrawString(Text, DFont, TextColor, New Point(27, 1))
                        End Using

                    End If

                End Using

            End If

            If Checked Then

                If Enabled Then

                    Using I As Image = Image.FromStream(New IO.MemoryStream(Convert.FromBase64String(B64E)))
                        G.DrawImage(I, New Rectangle(5, 4, 12, 12))
                    End Using

                Else

                    Using I As Image = Image.FromStream(New IO.MemoryStream(Convert.FromBase64String(B64D)))
                        G.DrawImage(I, New Rectangle(5, 4, 12, 12))
                    End Using

                End If

            End If

            MyBase.OnPaint(e)
        End Sub

    End Class

    <DefaultEvent("CheckedChanged")>
    Public Class AirFoxRadioButton
        Inherits Base.BaseRadioButton

        Private G As Graphics
        Public Property Bold As Boolean

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            G.Clear(Parent.BackColor)

            If Enabled Then

                Select Case State

                    Case MouseState.None

                        Using Border As New Pen(ColorFromHex("#C8C8C8"))
                            G.DrawEllipse(Border, New Rectangle(0, 0, 20, 20))
                        End Using

                    Case Else

                        Using Border As New Pen(ColorFromHex("#2C9CDA"))
                            G.DrawEllipse(Border, New Rectangle(0, 0, 20, 20))
                        End Using

                End Select

                Using TextColor As New SolidBrush(ColorFromHex("#424E5A"))

                    If Bold Then

                        Using BFont As New Font("Segoe UI", 10, FontStyle.Bold)
                            G.DrawString(Text, BFont, TextColor, New Point(27, 1))
                        End Using

                    Else
                        Using DFont As New Font("Segoe UI", 10)
                            G.DrawString(Text, DFont, TextColor, New Point(27, 1))
                        End Using

                    End If

                End Using

            Else

                Using Border As New Pen(ColorFromHex("#E6E6E6"))
                    G.DrawEllipse(Border, New Rectangle(0, 0, 20, 20))
                End Using

                Using TextColor As New SolidBrush(ColorFromHex("#A6B2BE"))

                    If Bold Then

                        Using BFont As New Font("Segoe UI", 10, FontStyle.Bold)
                            G.DrawString(Text, BFont, TextColor, New Point(27, 1))
                        End Using

                    Else
                        Using DFont As New Font("Segoe UI", 10)
                            G.DrawString(Text, DFont, TextColor, New Point(27, 1))
                        End Using

                    End If

                End Using

            End If

            If Checked Then

                If Enabled Then

                    Using FillColor As New SolidBrush(ColorFromHex("#2C9CDA"))
                        G.FillEllipse(FillColor, New Rectangle(4, 4, 12, 12))
                    End Using

                Else

                    Using FillColor As New SolidBrush(ColorFromHex("#B6B4B4"))
                        G.FillEllipse(FillColor, New Rectangle(4, 4, 12, 12))
                    End Using

                End If

            End If

            MyBase.OnPaint(e)
        End Sub

    End Class

    Public Class AirFoxHeader1
        Inherits Control

        Private G As Graphics

        Sub New()
            Font = New Font("Segoe UI Semibold", 20)
            ForeColor = ColorFromHex("#4C5864")
            DoubleBuffered = True
        End Sub

        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Size = New Size(Width, 51)
        End Sub

        Protected Overrides Sub OnPaint(e As PaintEventArgs)

            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            G.Clear(Parent.BackColor)

            Using HFont As New Font("Segoe UI Semibold", 20), HColor As New SolidBrush(ColorFromHex("#4C5864"))
                G.DrawString(Text, HFont, HColor, New Point(0, 0))
            End Using

            Using BottomLine As New Pen(ColorFromHex("#C8C8C8"))
                G.DrawLine(BottomLine, New Point(0, 50), New Point(Width, 50))
            End Using

            MyBase.OnPaint(e)

        End Sub

    End Class

    Public Class AirFoxHeader2
        Inherits Control

        Private G As Graphics

        Sub New()
            Font = New Font("Segoe UI", 10, FontStyle.Bold)
            ForeColor = ColorFromHex("#4C5864")
            DoubleBuffered = True
        End Sub

        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Size = New Size(Width, 19)
        End Sub

        Protected Overrides Sub OnPaint(e As PaintEventArgs)

            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            G.Clear(Parent.BackColor)

            Using HFont As New Font("Segoe UI", 10, FontStyle.Bold), HColor As New SolidBrush(ColorFromHex("#4C5864"))
                G.DrawString(Text, HFont, HColor, New Point(0, 0))
            End Using

            MyBase.OnPaint(e)

        End Sub

    End Class

    <DefaultEvent("CheckedChanged")>
    Public Class AirFoxOnOffBox
        Inherits Base.CheckControl

        Private G As Graphics
        Private B64C As String = "iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAQAAAAnOwc2AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQffCxwIKTQpQueKAAAAn0lEQVQI1yXKMU4CQRxG8TczW5nYWRCX+4it1/AUVvacArkGBQkBLmKUkBB3ne/b+VNs9ZKXXwKAOicT8cR3mVejUbo0scpf/NKSypRE7Sr1VReFdgx55D+rE3Wlq0J798SD3qeFqC+6KHR2b9BGoa3e9KPQwUvjgtYKNY0KnfxsVCr84Q+FQsdZGcOQB/ypgxezqhgi3VIr02PDyRgDd6AdcPpYOg4ZAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE1LTExLTI4VDA4OjQxOjUyLTA1OjAwH7rbKgAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNS0xMS0yOFQwODo0MTo1Mi0wNTowMG7nY5YAAAAASUVORK5CYII="
        Private B64U As String = "iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAQAAAAnOwc2AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQffCxwIKir4YIkqAAAAgUlEQVQI122OMQrCQAAENxoMxz3Aj8Y3WAv6jtzVAYPYKah/8AtC5AZdm1TqFss0y6xGseXoxb26yA172iKx5o1JDg4kzMhK9JgnJpMn6uVIwoCn7hx1lmsSplAwyfVJs2Wlr8wlR7qfOYc/Ina8MNnBgTxdeogNg5ubrnLDQFv0AXVYjzifEiowAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE1LTExLTI4VDA4OjQyOjQyLTA1OjAwOCdgtwAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNS0xMS0yOFQwODo0Mjo0Mi0wNTowMEl62AsAAAAASUVORK5CYII="

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            If Enabled Then

                If Checked Then

                    Using Border As New Pen(ColorFromHex("#C8C8C8")), Background As New SolidBrush(ColorFromHex("#2C9CDA")), BackBorder As New Pen(ColorFromHex("#2A8AC1")), I As Image = Image.FromStream(New IO.MemoryStream(Convert.FromBase64String(B64C)))
                        G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))

                        G.FillPath(Background, RoundRect(New Rectangle(5, 5, 17, 17), 2))
                        G.DrawPath(BackBorder, RoundRect(New Rectangle(5, 5, 17, 17), 2))

                        G.DrawImage(I, New Point(9, 9))
                    End Using

                Else

                    Using Border As New Pen(ColorFromHex("#C8C8C8")), Background As New SolidBrush(ColorFromHex("#FF9500")), BackBorder As New Pen(ColorFromHex("#DC8400")), I As Image = Image.FromStream(New IO.MemoryStream(Convert.FromBase64String(B64U)))
                        G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))

                        G.FillPath(Background, RoundRect(New Rectangle(Width - 23, 5, 17, 17), 2))
                        G.DrawPath(BackBorder, RoundRect(New Rectangle(Width - 23, 5, 17, 17), 2))

                        G.DrawImage(I, New Point(Width - 19, 9))
                    End Using

                End If

            Else

                If Checked Then

                    Using Border As New Pen(ColorFromHex("#E6E6E6")), Background As New SolidBrush(ColorFromHex("#7DB7D8")), BackBorder As New Pen(ColorFromHex("#7CA6BF")), I As Image = Image.FromStream(New IO.MemoryStream(Convert.FromBase64String(B64C)))
                        G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))

                        G.FillPath(Background, RoundRect(New Rectangle(5, 5, 17, 17), 2))
                        G.DrawPath(BackBorder, RoundRect(New Rectangle(5, 5, 17, 17), 2))

                        G.DrawImage(I, New Point(9, 9))
                    End Using

                Else

                    Using Border As New Pen(ColorFromHex("#E6E6E6")), Background As New SolidBrush(ColorFromHex("#FFCB7C")), BackBorder As New Pen(ColorFromHex("#E2BD85")), I As Image = Image.FromStream(New IO.MemoryStream(Convert.FromBase64String(B64U)))
                        G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))

                        G.FillPath(Background, RoundRect(New Rectangle(Width - 23, 5, 17, 17), 2))
                        G.DrawPath(BackBorder, RoundRect(New Rectangle(Width - 23, 5, 17, 17), 2))

                        G.DrawImage(I, New Point(Width - 19, 9))
                    End Using

                End If


            End If


            MyBase.OnPaint(e)
        End Sub

        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Size = New Size(55, 28)
        End Sub

    End Class

    Public Class AirFoxButton
        Inherits Base.ButtonBase

        Private G As Graphics

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            G.Clear(Parent.BackColor)

            If Enabled Then

                Select Case State

                    Case MouseState.None

                        Using Background As New SolidBrush(ColorFromHex("#F9F9F9"))
                            G.FillPath(Background, RoundRect(FullRectangle(Size, True), 2))
                        End Using

                    Case MouseState.Over

                        Using Background As New SolidBrush(ColorFromHex("#F2F2F2"))
                            G.FillPath(Background, RoundRect(FullRectangle(Size, True), 2))
                        End Using

                    Case MouseState.Down

                        Using Background As New SolidBrush(ColorFromHex("#E8E8E8"))
                            G.FillPath(Background, RoundRect(FullRectangle(Size, True), 2))
                        End Using

                End Select

                Using Border As New Pen(ColorFromHex("#C1C1C1"))
                    G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))
                End Using

                Using TextFont As New Font("Segoe UI", 10), TextColor As New SolidBrush(ColorFromHex("#424E5A"))
                    CenterString(G, Text, TextFont, TextColor.Color, New Rectangle(3, 0, Width, Height))
                End Using

            Else

                Using Background As New SolidBrush(ColorFromHex("#F9F9F9"))
                    G.FillPath(Background, RoundRect(FullRectangle(Size, True), 2))
                End Using

                Using TextFont As New Font("Segoe UI", 10), TextColor As New SolidBrush(ColorFromHex("#A6B2BE"))
                    CenterString(G, Text, TextFont, TextColor.Color, New Rectangle(3, 0, Width, Height))
                End Using

                Using Border As New Pen(ColorFromHex("#D1D1D1"))
                    G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))
                End Using

            End If

            MyBase.OnPaint(e)
        End Sub

    End Class

    <DefaultEvent("TextChanged")>
    Public Class AirFoxTextbox
        Inherits Control

        Private WithEvents TB As New TextBox
        Private G As Graphics
        Private State As MouseState
        Private IsDown As Boolean
        Private _allowpassword As Boolean = False
        Private _maxChars As Integer = 32767
        Private _textAlignment As HorizontalAlignment
        Private _multiLine As Boolean = False
        Private _readOnly As Boolean = False

        Private IsEnabled As Boolean

        Public Shadows Property Enabled As Boolean
            Get
                Return EnabledCalc
            End Get
            Set(value As Boolean)
                IsEnabled = value

                If Enabled Then
                    Cursor = Cursors.Hand
                Else
                    Cursor = Cursors.Default
                End If

                Invalidate()
            End Set
        End Property

        <DisplayName("Enabled")>
        Public Property EnabledCalc As Boolean
            Get
                Return IsEnabled
            End Get
            Set(value As Boolean)
                Enabled = value
                Invalidate()
            End Set
        End Property

        Public Shadows Property UseSystemPasswordChar() As Boolean
            Get
                Return _allowpassword
            End Get
            Set(ByVal value As Boolean)
                TB.UseSystemPasswordChar = UseSystemPasswordChar
                _allowpassword = value
                Invalidate()
            End Set
        End Property

        Public Shadows Property MaxLength() As Integer
            Get
                Return _maxChars
            End Get
            Set(ByVal value As Integer)
                _maxChars = value
                TB.MaxLength = MaxLength
                Invalidate()
            End Set
        End Property

        Public Shadows Property TextAlign() As HorizontalAlignment
            Get
                Return _textAlignment
            End Get
            Set(ByVal value As HorizontalAlignment)
                _textAlignment = value
                Invalidate()
            End Set
        End Property

        Public Shadows Property MultiLine() As Boolean
            Get
                Return _multiLine
            End Get
            Set(ByVal value As Boolean)
                _multiLine = value
                TB.Multiline = value
                OnResize(EventArgs.Empty)
                Invalidate()
            End Set
        End Property

        Public Shadows Property [ReadOnly]() As Boolean
            Get
                Return _readOnly
            End Get
            Set(ByVal value As Boolean)
                _readOnly = value
                If TB IsNot Nothing Then
                    TB.ReadOnly = value
                End If
            End Set
        End Property

        Protected Overrides Sub OnTextChanged(ByVal e As EventArgs)
            MyBase.OnTextChanged(e)
            Invalidate()
        End Sub

        Protected Overrides Sub OnBackColorChanged(ByVal e As EventArgs)
            MyBase.OnBackColorChanged(e)
            Invalidate()
        End Sub

        Protected Overrides Sub OnForeColorChanged(ByVal e As EventArgs)
            MyBase.OnForeColorChanged(e)
            TB.ForeColor = ForeColor
            Invalidate()
        End Sub

        Protected Overrides Sub OnFontChanged(ByVal e As EventArgs)
            MyBase.OnFontChanged(e)
            TB.Font = Font
        End Sub

        Protected Overrides Sub OnGotFocus(ByVal e As EventArgs)
            MyBase.OnGotFocus(e)
            TB.Focus()
        End Sub

        Private Sub TextChangeTb() Handles TB.TextChanged
            Text = TB.Text
        End Sub

        Private Sub TextChng() Handles MyBase.TextChanged
            TB.Text = Text
        End Sub

        Public Sub NewTextBox()
            With TB
                .Text = String.Empty
                .BackColor = Color.White
                .ForeColor = Color.FromArgb(66, 78, 90)
                .TextAlign = HorizontalAlignment.Left
                .BorderStyle = BorderStyle.None
                .Location = New Point(3, 3)
                .Font = New Font("Segoe UI", 10)
                .Size = New Size(Width - 3, Height - 3)
                .UseSystemPasswordChar = UseSystemPasswordChar
            End With
        End Sub

        Sub New()
            MyBase.New()
            NewTextBox()
            Controls.Add(TB)
            SetStyle(ControlStyles.UserPaint Or ControlStyles.SupportsTransparentBackColor, True)
            DoubleBuffered = True
            TextAlign = HorizontalAlignment.Left
            ForeColor = Color.FromArgb(66, 78, 90)
            Font = New Font("Segoe UI", 10)
            Size = New Size(130, 29)
            Enabled = True
        End Sub

        Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)

            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            G.Clear(Parent.BackColor)

            If Enabled Then

                If State = MouseState.Down Then

                    Using Border As New Pen(ColorFromHex("#2C9CDA"))
                        G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))
                    End Using

                Else

                    Using Border As New Pen(ColorFromHex("#C8C8C8"))
                        G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))
                    End Using

                End If

            Else

                Using Border As New Pen(ColorFromHex("#E6E6E6"))
                    G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))
                End Using

            End If

            TB.TextAlign = TextAlign
            TB.UseSystemPasswordChar = UseSystemPasswordChar

            MyBase.OnPaint(e)

        End Sub

        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            If Not MultiLine Then
                TB.Location = New Point(10, (Height / 2) - (TB.Height / 2) - 0)
                TB.Size = New Size(Width - 20, TB.Height)
            Else
                TB.Location = New Point(10, 10)
                TB.Size = New Size(Width - 20, Height - 20)
            End If
        End Sub

        Protected Overrides Sub OnEnter(e As EventArgs)
            MyBase.OnEnter(e)
            State = MouseState.Down : Invalidate()
        End Sub

        Protected Overrides Sub OnLeave(e As EventArgs)
            MyBase.OnLeave(e)
            State = MouseState.None : Invalidate()
        End Sub

    End Class

    Public Class AirFoxNumericUpDown
        Inherits Control

        Private G As Graphics
        Private IsEnabled As Boolean

        Private _Value As Integer
        Public Property Value As Integer
            Get
                Return _Value
            End Get
            Set(value As Integer)
                Select Case value
                    Case Is > Max
                        value = Max
                        Invalidate()

                    Case Is < Min
                        value = Min
                        Invalidate()
                End Select
                _Value = value
                Invalidate()
            End Set
        End Property

        Private _Max As Integer = 100
        Public Property Max As Integer
            Get
                Return _Max
            End Get
            Set(value As Integer)
                Select Case value
                    Case Is < _Value
                        _Value = value
                End Select
                _Max = value
                Invalidate()
            End Set
        End Property

        Private _Min As Integer
        Public Property Min As Integer
            Get
                Return _Min
            End Get
            Set(value As Integer)
                Select Case value
                    Case Is > _Value
                        _Value = value
                End Select
                _Min = value
                Invalidate()
            End Set
        End Property

        Public Shadows Property Enabled As Boolean
            Get
                Return EnabledCalc
            End Get
            Set(value As Boolean)
                IsEnabled = value
                Invalidate()
            End Set
        End Property

        <DisplayName("Enabled")>
        Public Property EnabledCalc As Boolean
            Get
                Return IsEnabled
            End Get
            Set(value As Boolean)
                Enabled = value
                Invalidate()
            End Set
        End Property

        Sub New()
            Enabled = True
            DoubleBuffered = True
        End Sub

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            If Enabled Then

                Using Border As New Pen(ColorFromHex("#C8C8C8"))
                    G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))
                    G.DrawPath(Border, RoundRect(New Rectangle(Width - 20, 4, 15, 18), 2))
                End Using

                Using TextColor As New SolidBrush(ColorFromHex("#424E5A")), TextFont As New Font("Segoe UI", 10)
                    CenterString(G, Value, TextFont, TextColor.Color, New Rectangle(-10, 0, Width, Height))
                End Using

                Using SignColor As New SolidBrush(ColorFromHex("#56626E")), SignFont As New Font("Marlett", 10)
                    G.DrawString("t", SignFont, SignColor, New Point(Width - 20, 4))
                    G.DrawString("u", SignFont, SignColor, New Point(Width - 20, 10))
                End Using

            Else

                Using Border As New Pen(ColorFromHex("#E6E6E6"))
                    G.DrawPath(Border, RoundRect(FullRectangle(Size, True), 2))
                    G.DrawPath(Border, RoundRect(New Rectangle(Width - 20, 4, 15, 18), 2))
                End Using

                Using TextColor As New SolidBrush(ColorFromHex("#A6B2BE")), TextFont As New Font("Segoe UI", 10)
                    CenterString(G, Value, TextFont, TextColor.Color, New Rectangle(-10, 0, Width, Height))
                End Using

                Using SignColor As New SolidBrush(ColorFromHex("#BAC6D2")), SignFont As New Font("Marlett", 10)
                    G.DrawString("t", SignFont, SignColor, New Point(Width - 20, 4))
                    G.DrawString("u", SignFont, SignColor, New Point(Width - 20, 10))
                End Using

            End If

            MyBase.OnPaint(e)
        End Sub

        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)

            If Enabled Then

                If e.X > Width - 20 And e.Y < 10 Then
                    Value += 1
                ElseIf e.X > Width - 20 And e.Y > 10 Then
                    Value -= 1
                End If

            End If

        End Sub

        Protected Overrides Sub OnMouseMove(e As MouseEventArgs)
            MyBase.OnMouseMove(e)

            If Enabled Then

                If e.X > Width - 20 And e.Y < 10 Then
                    Cursor = Cursors.Hand
                ElseIf e.X > Width - 20 And e.Y > 10 Then
                    Cursor = Cursors.Hand
                Else
                    Cursor = Cursors.Default
                End If

            End If

        End Sub

        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Size = New Size(Width, 27)
        End Sub

    End Class

    Public Class AirFoxRedirect
        Inherits Base.ButtonBase

        Private G As Graphics

        Protected Overrides Sub OnPaint(e As PaintEventArgs)

            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            Select Case State

                Case MouseState.Over

                    Using TextColor As New SolidBrush(ColorFromHex("#178CE5")), TextFont As New Font("Segoe UI", 10, FontStyle.Underline)
                        G.DrawString(Text, TextFont, TextColor, New Point(0, 0))
                    End Using

                Case MouseState.Down

                    Using TextColor As New SolidBrush(ColorFromHex("#FF9500")), TextFont As New Font("Segoe UI", 10)
                        G.DrawString(Text, TextFont, TextColor, New Point(0, 0))
                    End Using

                Case Else

                    Using TextColor As New SolidBrush(ColorFromHex("#0095DD")), TextFont As New Font("Segoe UI", 10)
                        G.DrawString(Text, TextFont, TextColor, New Point(0, 0))
                    End Using

            End Select

            MyBase.OnPaint(e)

        End Sub

    End Class

    Public Class AirFoxAlertBox
        Inherits Base.ButtonBase

        Public Property Style As Styles = Styles.Green

        Private G As Graphics
        Private Background As Color
        Private TextColor As Color
        Private LeftBar As Color

        Public Enum Styles As Byte
            Green = 0
            Blue = 1
            Yellow = 2
            Red = 3
        End Enum

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            G = e.Graphics
            G.SmoothingMode = SmoothingMode.HighQuality
            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit

            Select Case Style

                Case Styles.Green
                    Background = ColorFromHex("#DFF0D6")
                    TextColor = ColorFromHex("#4E8C45")
                    LeftBar = ColorFromHex("#CEE5B6")

                Case Styles.Blue
                    Background = ColorFromHex("#D9EDF8")
                    TextColor = ColorFromHex("#498FB8")
                    LeftBar = ColorFromHex("#AFD9F0")

                Case Styles.Yellow
                    Background = ColorFromHex("#FCF8E1")
                    TextColor = ColorFromHex("#908358")
                    LeftBar = ColorFromHex("#FAEBC8")

                Case Styles.Red
                    Background = ColorFromHex("#F2DEDE")
                    TextColor = ColorFromHex("#C2635E")
                    LeftBar = ColorFromHex("#EBCCD1")

            End Select

            Using TextFont As New Font("Segoe UI", 10), Back As New SolidBrush(Background), TC As New SolidBrush(TextColor), LB As New SolidBrush(LeftBar)
                G.FillRectangle(Back, FullRectangle(Size, True))
                G.SmoothingMode = SmoothingMode.None
                G.FillRectangle(LB, New Rectangle(0, 1, 6, Height - 2))
                G.SmoothingMode = SmoothingMode.HighQuality
                G.DrawString(Text, TextFont, TC, New Point(20, 11))
            End Using

            MyBase.OnPaint(e)
        End Sub

        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Size = New Size(Width, 40)
        End Sub

    End Class

End Namespace