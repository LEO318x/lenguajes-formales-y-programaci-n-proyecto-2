﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_Desarrollador
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Desarrollador))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevaPestañaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CerrarPestañaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbrirArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnalizarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HTMLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManualDeUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcercaDeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListViewTokens = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ListViewErroresLexicos = New System.Windows.Forms.ListView()
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ListViewErroresSintacticos = New System.Windows.Forms.ListView()
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader15 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btn_CerrarPestaña = New System.Windows.Forms.Button()
        Me.btn_PestañaNueva = New System.Windows.Forms.Button()
        Me.btn_restart = New Proyecto_2__LFP_1S2016_.Theme.AirFoxButton()
        Me.BonfireAlertBox2 = New Proyecto_2__LFP_1S2016_.BonfireAlertBox()
        Me.BonfireAlertBox1 = New Proyecto_2__LFP_1S2016_.BonfireAlertBox()
        Me.BonfireGroupBox1 = New Proyecto_2__LFP_1S2016_.BonfireGroupBox()
        Me.btn_salidaHTML = New Proyecto_2__LFP_1S2016_.BonfireButton()
        Me.btn_analizarDatos = New Proyecto_2__LFP_1S2016_.BonfireButton()
        Me.btn_guardarInfo = New Proyecto_2__LFP_1S2016_.BonfireButton()
        Me.btn_abrirArchivo = New Proyecto_2__LFP_1S2016_.BonfireButton()
        Me.AirFoxHeader21 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxHeader2()
        Me.TabControl_Pestañas = New Proyecto_2__LFP_1S2016_.BonfireTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtPestaña = New System.Windows.Forms.TextBox()
        Me.MenuStrip1.SuspendLayout()
        Me.BonfireGroupBox1.SuspendLayout()
        Me.TabControl_Pestañas.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.AyudaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(839, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevaPestañaToolStripMenuItem, Me.CerrarPestañaToolStripMenuItem, Me.AbrirArchivoToolStripMenuItem, Me.GuardarToolStripMenuItem, Me.AnalizarToolStripMenuItem, Me.HTMLToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'NuevaPestañaToolStripMenuItem
        '
        Me.NuevaPestañaToolStripMenuItem.Name = "NuevaPestañaToolStripMenuItem"
        Me.NuevaPestañaToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.NuevaPestañaToolStripMenuItem.Text = "&Nueva Pestaña"
        '
        'CerrarPestañaToolStripMenuItem
        '
        Me.CerrarPestañaToolStripMenuItem.Name = "CerrarPestañaToolStripMenuItem"
        Me.CerrarPestañaToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.CerrarPestañaToolStripMenuItem.Text = "&Cerrar Pestaña"
        '
        'AbrirArchivoToolStripMenuItem
        '
        Me.AbrirArchivoToolStripMenuItem.Name = "AbrirArchivoToolStripMenuItem"
        Me.AbrirArchivoToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AbrirArchivoToolStripMenuItem.Text = "&Abrir Archivo"
        '
        'GuardarToolStripMenuItem
        '
        Me.GuardarToolStripMenuItem.Name = "GuardarToolStripMenuItem"
        Me.GuardarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.GuardarToolStripMenuItem.Text = "&Guardar"
        '
        'AnalizarToolStripMenuItem
        '
        Me.AnalizarToolStripMenuItem.Name = "AnalizarToolStripMenuItem"
        Me.AnalizarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AnalizarToolStripMenuItem.Text = "&Analizar"
        '
        'HTMLToolStripMenuItem
        '
        Me.HTMLToolStripMenuItem.Name = "HTMLToolStripMenuItem"
        Me.HTMLToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.HTMLToolStripMenuItem.Text = "&HTML"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SalirToolStripMenuItem.Text = "&Salir"
        '
        'AyudaToolStripMenuItem
        '
        Me.AyudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ManualDeUsuarioToolStripMenuItem, Me.AcercaDeToolStripMenuItem})
        Me.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem"
        Me.AyudaToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.AyudaToolStripMenuItem.Text = "&Ayuda"
        '
        'ManualDeUsuarioToolStripMenuItem
        '
        Me.ManualDeUsuarioToolStripMenuItem.Name = "ManualDeUsuarioToolStripMenuItem"
        Me.ManualDeUsuarioToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.ManualDeUsuarioToolStripMenuItem.Text = "&Manual de Usuario"
        '
        'AcercaDeToolStripMenuItem
        '
        Me.AcercaDeToolStripMenuItem.Name = "AcercaDeToolStripMenuItem"
        Me.AcercaDeToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.AcercaDeToolStripMenuItem.Text = "&Acerca de..."
        '
        'ListViewTokens
        '
        Me.ListViewTokens.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6})
        Me.ListViewTokens.Location = New System.Drawing.Point(458, 301)
        Me.ListViewTokens.Name = "ListViewTokens"
        Me.ListViewTokens.Size = New System.Drawing.Size(370, 97)
        Me.ListViewTokens.TabIndex = 5
        Me.ListViewTokens.UseCompatibleStateImageBehavior = False
        Me.ListViewTokens.View = System.Windows.Forms.View.Details
        Me.ListViewTokens.Visible = False
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "#"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Fila"
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Columna"
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Lexema"
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "IdToken"
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Token"
        '
        'ListViewErroresLexicos
        '
        Me.ListViewErroresLexicos.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader10, Me.ColumnHeader11})
        Me.ListViewErroresLexicos.Location = New System.Drawing.Point(458, 404)
        Me.ListViewErroresLexicos.Name = "ListViewErroresLexicos"
        Me.ListViewErroresLexicos.Size = New System.Drawing.Size(370, 97)
        Me.ListViewErroresLexicos.TabIndex = 7
        Me.ListViewErroresLexicos.UseCompatibleStateImageBehavior = False
        Me.ListViewErroresLexicos.View = System.Windows.Forms.View.Details
        Me.ListViewErroresLexicos.Visible = False
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "#"
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Fila"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Columna"
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Caracter"
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Descripcion"
        Me.ColumnHeader11.Width = 95
        '
        'ListViewErroresSintacticos
        '
        Me.ListViewErroresSintacticos.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader12, Me.ColumnHeader13, Me.ColumnHeader14, Me.ColumnHeader15})
        Me.ListViewErroresSintacticos.Location = New System.Drawing.Point(458, 507)
        Me.ListViewErroresSintacticos.Name = "ListViewErroresSintacticos"
        Me.ListViewErroresSintacticos.Size = New System.Drawing.Size(369, 99)
        Me.ListViewErroresSintacticos.TabIndex = 8
        Me.ListViewErroresSintacticos.UseCompatibleStateImageBehavior = False
        Me.ListViewErroresSintacticos.View = System.Windows.Forms.View.Details
        Me.ListViewErroresSintacticos.Visible = False
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "#"
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Fila"
        '
        'ColumnHeader14
        '
        Me.ColumnHeader14.Text = "Columna"
        Me.ColumnHeader14.Width = 106
        '
        'ColumnHeader15
        '
        Me.ColumnHeader15.Text = "Descripción"
        Me.ColumnHeader15.Width = 148
        '
        'btn_CerrarPestaña
        '
        Me.btn_CerrarPestaña.Image = CType(resources.GetObject("btn_CerrarPestaña.Image"), System.Drawing.Image)
        Me.btn_CerrarPestaña.Location = New System.Drawing.Point(453, 127)
        Me.btn_CerrarPestaña.Name = "btn_CerrarPestaña"
        Me.btn_CerrarPestaña.Size = New System.Drawing.Size(44, 44)
        Me.btn_CerrarPestaña.TabIndex = 4
        Me.btn_CerrarPestaña.UseVisualStyleBackColor = True
        '
        'btn_PestañaNueva
        '
        Me.btn_PestañaNueva.Image = CType(resources.GetObject("btn_PestañaNueva.Image"), System.Drawing.Image)
        Me.btn_PestañaNueva.Location = New System.Drawing.Point(453, 77)
        Me.btn_PestañaNueva.Name = "btn_PestañaNueva"
        Me.btn_PestañaNueva.Size = New System.Drawing.Size(44, 44)
        Me.btn_PestañaNueva.TabIndex = 3
        Me.btn_PestañaNueva.UseVisualStyleBackColor = True
        '
        'btn_restart
        '
        Me.btn_restart.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_restart.EnabledCalc = True
        Me.btn_restart.Location = New System.Drawing.Point(709, 27)
        Me.btn_restart.Name = "btn_restart"
        Me.btn_restart.Size = New System.Drawing.Size(126, 23)
        Me.btn_restart.TabIndex = 11
        Me.btn_restart.Text = "Reiniciar Aplicación"
        '
        'BonfireAlertBox2
        '
        Me.BonfireAlertBox2.AlertStyle = Proyecto_2__LFP_1S2016_.BonfireAlertBox.Style._Success
        Me.BonfireAlertBox2.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.BonfireAlertBox2.Location = New System.Drawing.Point(458, 235)
        Me.BonfireAlertBox2.Name = "BonfireAlertBox2"
        Me.BonfireAlertBox2.Size = New System.Drawing.Size(369, 46)
        Me.BonfireAlertBox2.TabIndex = 10
        Me.BonfireAlertBox2.Text = "Se han agreagado cuentos"
        Me.BonfireAlertBox2.Visible = False
        '
        'BonfireAlertBox1
        '
        Me.BonfireAlertBox1.AlertStyle = Proyecto_2__LFP_1S2016_.BonfireAlertBox.Style._Error
        Me.BonfireAlertBox1.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.BonfireAlertBox1.Location = New System.Drawing.Point(458, 235)
        Me.BonfireAlertBox1.Name = "BonfireAlertBox1"
        Me.BonfireAlertBox1.Size = New System.Drawing.Size(369, 46)
        Me.BonfireAlertBox1.TabIndex = 9
        Me.BonfireAlertBox1.Text = "LexicoSintactico Cuentos NO Generados"
        Me.BonfireAlertBox1.Visible = False
        '
        'BonfireGroupBox1
        '
        Me.BonfireGroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.BonfireGroupBox1.Controls.Add(Me.btn_salidaHTML)
        Me.BonfireGroupBox1.Controls.Add(Me.btn_analizarDatos)
        Me.BonfireGroupBox1.Controls.Add(Me.btn_guardarInfo)
        Me.BonfireGroupBox1.Controls.Add(Me.btn_abrirArchivo)
        Me.BonfireGroupBox1.Controls.Add(Me.AirFoxHeader21)
        Me.BonfireGroupBox1.Location = New System.Drawing.Point(513, 99)
        Me.BonfireGroupBox1.Name = "BonfireGroupBox1"
        Me.BonfireGroupBox1.Size = New System.Drawing.Size(283, 130)
        Me.BonfireGroupBox1.TabIndex = 2
        Me.BonfireGroupBox1.Text = "BonfireGroupBox1"
        '
        'btn_salidaHTML
        '
        Me.btn_salidaHTML.ButtonStyle = Proyecto_2__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btn_salidaHTML.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_salidaHTML.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btn_salidaHTML.Image = CType(resources.GetObject("btn_salidaHTML.Image"), System.Drawing.Image)
        Me.btn_salidaHTML.Location = New System.Drawing.Point(163, 75)
        Me.btn_salidaHTML.Name = "btn_salidaHTML"
        Me.btn_salidaHTML.RoundedCorners = False
        Me.btn_salidaHTML.Size = New System.Drawing.Size(93, 37)
        Me.btn_salidaHTML.TabIndex = 4
        Me.btn_salidaHTML.Text = "HTML"
        '
        'btn_analizarDatos
        '
        Me.btn_analizarDatos.ButtonStyle = Proyecto_2__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btn_analizarDatos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_analizarDatos.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btn_analizarDatos.Image = CType(resources.GetObject("btn_analizarDatos.Image"), System.Drawing.Image)
        Me.btn_analizarDatos.Location = New System.Drawing.Point(28, 75)
        Me.btn_analizarDatos.Name = "btn_analizarDatos"
        Me.btn_analizarDatos.RoundedCorners = False
        Me.btn_analizarDatos.Size = New System.Drawing.Size(93, 37)
        Me.btn_analizarDatos.TabIndex = 3
        Me.btn_analizarDatos.Text = "Analizar"
        '
        'btn_guardarInfo
        '
        Me.btn_guardarInfo.ButtonStyle = Proyecto_2__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btn_guardarInfo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_guardarInfo.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btn_guardarInfo.Image = CType(resources.GetObject("btn_guardarInfo.Image"), System.Drawing.Image)
        Me.btn_guardarInfo.Location = New System.Drawing.Point(163, 28)
        Me.btn_guardarInfo.Name = "btn_guardarInfo"
        Me.btn_guardarInfo.RoundedCorners = False
        Me.btn_guardarInfo.Size = New System.Drawing.Size(93, 37)
        Me.btn_guardarInfo.TabIndex = 2
        Me.btn_guardarInfo.Text = "Guardar"
        '
        'btn_abrirArchivo
        '
        Me.btn_abrirArchivo.ButtonStyle = Proyecto_2__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btn_abrirArchivo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_abrirArchivo.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btn_abrirArchivo.Image = CType(resources.GetObject("btn_abrirArchivo.Image"), System.Drawing.Image)
        Me.btn_abrirArchivo.Location = New System.Drawing.Point(28, 28)
        Me.btn_abrirArchivo.Name = "btn_abrirArchivo"
        Me.btn_abrirArchivo.RoundedCorners = False
        Me.btn_abrirArchivo.Size = New System.Drawing.Size(93, 37)
        Me.btn_abrirArchivo.TabIndex = 1
        Me.btn_abrirArchivo.Text = "Abrir"
        '
        'AirFoxHeader21
        '
        Me.AirFoxHeader21.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.AirFoxHeader21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.AirFoxHeader21.Location = New System.Drawing.Point(103, 3)
        Me.AirFoxHeader21.Name = "AirFoxHeader21"
        Me.AirFoxHeader21.Size = New System.Drawing.Size(75, 19)
        Me.AirFoxHeader21.TabIndex = 0
        Me.AirFoxHeader21.Text = "Funciones"
        '
        'TabControl_Pestañas
        '
        Me.TabControl_Pestañas.Controls.Add(Me.TabPage1)
        Me.TabControl_Pestañas.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.TabControl_Pestañas.ItemSize = New System.Drawing.Size(0, 30)
        Me.TabControl_Pestañas.Location = New System.Drawing.Point(12, 37)
        Me.TabControl_Pestañas.Name = "TabControl_Pestañas"
        Me.TabControl_Pestañas.SelectedIndex = 0
        Me.TabControl_Pestañas.Size = New System.Drawing.Size(439, 560)
        Me.TabControl_Pestañas.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.txtPestaña)
        Me.TabPage1.Location = New System.Drawing.Point(4, 34)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(431, 522)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Pestaña 1"
        '
        'txtPestaña
        '
        Me.txtPestaña.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPestaña.Location = New System.Drawing.Point(6, 6)
        Me.txtPestaña.MaxLength = 10000000
        Me.txtPestaña.Multiline = True
        Me.txtPestaña.Name = "txtPestaña"
        Me.txtPestaña.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtPestaña.Size = New System.Drawing.Size(419, 510)
        Me.txtPestaña.TabIndex = 0
        '
        'frm_Desarrollador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(839, 609)
        Me.Controls.Add(Me.btn_restart)
        Me.Controls.Add(Me.BonfireAlertBox2)
        Me.Controls.Add(Me.BonfireAlertBox1)
        Me.Controls.Add(Me.ListViewErroresSintacticos)
        Me.Controls.Add(Me.ListViewErroresLexicos)
        Me.Controls.Add(Me.ListViewTokens)
        Me.Controls.Add(Me.btn_CerrarPestaña)
        Me.Controls.Add(Me.btn_PestañaNueva)
        Me.Controls.Add(Me.BonfireGroupBox1)
        Me.Controls.Add(Me.TabControl_Pestañas)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frm_Desarrollador"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Area del Desarrollador"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.BonfireGroupBox1.ResumeLayout(False)
        Me.TabControl_Pestañas.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NuevaPestañaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CerrarPestañaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AbrirArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AnalizarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HTMLToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ManualDeUsuarioToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AcercaDeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TabControl_Pestañas As BonfireTabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents GuardarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BonfireGroupBox1 As BonfireGroupBox
    Friend WithEvents btn_salidaHTML As BonfireButton
    Friend WithEvents btn_analizarDatos As BonfireButton
    Friend WithEvents btn_guardarInfo As BonfireButton
    Friend WithEvents btn_abrirArchivo As BonfireButton
    Friend WithEvents AirFoxHeader21 As Theme.AirFoxHeader2
    Friend WithEvents btn_PestañaNueva As Button
    Friend WithEvents btn_CerrarPestaña As Button
    Friend WithEvents ListViewTokens As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents txtPestaña As TextBox
    Friend WithEvents ListViewErroresLexicos As ListView
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents ColumnHeader8 As ColumnHeader
    Friend WithEvents ColumnHeader9 As ColumnHeader
    Friend WithEvents ColumnHeader10 As ColumnHeader
    Friend WithEvents ColumnHeader11 As ColumnHeader
    Friend WithEvents ListViewErroresSintacticos As ListView
    Friend WithEvents ColumnHeader12 As ColumnHeader
    Friend WithEvents ColumnHeader13 As ColumnHeader
    Friend WithEvents ColumnHeader14 As ColumnHeader
    Friend WithEvents ColumnHeader15 As ColumnHeader
    Friend WithEvents BonfireAlertBox1 As BonfireAlertBox
    Friend WithEvents BonfireAlertBox2 As BonfireAlertBox
    Friend WithEvents btn_restart As Theme.AirFoxButton
End Class
