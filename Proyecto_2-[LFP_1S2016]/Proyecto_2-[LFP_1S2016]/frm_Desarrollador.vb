﻿Imports System.IO
Imports System.Reflection

Public Class frm_Desarrollador
    Private contadorPestaña As Integer = 2
    Public contadorTokens = 1
    Public contadorErrores = 1
    Public contadorLex = 1
    Public formulario As Object

    Private Sub frm_Desarrollador_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        cerrarFormulario(e)
    End Sub

    Private Sub NuevaPestañaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevaPestañaToolStripMenuItem.Click
        nuevaPestaña()
    End Sub

    Private Sub CerrarPestañaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarPestañaToolStripMenuItem.Click
        cerrarPestaña()
    End Sub

    Private Sub AbrirArchivoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirArchivoToolStripMenuItem.Click
        abrirArchivo()
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        guardarArchivo()
    End Sub

    Private Sub AnalizarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AnalizarToolStripMenuItem.Click
        analisisLexicoSintactico()
    End Sub

    Private Sub HTMLToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HTMLToolStripMenuItem.Click
        crearHTML(ListViewTokens, ListViewErroresLexicos, ListViewErroresSintacticos)
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        salirMenu()
    End Sub

    Private Sub ManualDeUsuarioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ManualDeUsuarioToolStripMenuItem.Click
        abrirManualdeUsuario()
    End Sub

    Private Sub AcercaDeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AcercaDeToolStripMenuItem.Click
        acercaDe()
    End Sub

    Private Sub btn_PestañaNueva_Click(sender As Object, e As EventArgs) Handles btn_PestañaNueva.Click
        nuevaPestaña()
    End Sub

    Private Sub btn_CerrarPestaña_Click(sender As Object, e As EventArgs) Handles btn_CerrarPestaña.Click
        cerrarPestaña()
    End Sub

    Private Sub btn_abrirArchivo_Click(sender As Object, e As EventArgs) Handles btn_abrirArchivo.Click
        abrirArchivo()
    End Sub

    Private Sub btn_guardarInfo_Click(sender As Object, e As EventArgs) Handles btn_guardarInfo.Click
        guardarArchivo()
    End Sub

    Private Sub btn_analizarDatos_Click(sender As Object, e As EventArgs) Handles btn_analizarDatos.Click
        analisisLexicoSintactico()
    End Sub

    Private Sub btn_salidaHTML_Click(sender As Object, e As EventArgs) Handles btn_salidaHTML.Click
        crearHTML(ListViewTokens, ListViewErroresLexicos, ListViewErroresSintacticos)
    End Sub


    '███████╗██╗   ██╗███╗   ██╗ ██████╗██╗ ██████╗ ███╗   ██╗███████╗███████╗
    '██╔════╝██║   ██║████╗  ██║██╔════╝██║██╔═══██╗████╗  ██║██╔════╝██╔════╝
    '█████╗  ██║   ██║██╔██╗ ██║██║     ██║██║   ██║██╔██╗ ██║█████╗  ███████╗
    '██╔══╝  ██║   ██║██║╚██╗██║██║     ██║██║   ██║██║╚██╗██║██╔══╝  ╚════██║
    '██║     ╚██████╔╝██║ ╚████║╚██████╗██║╚██████╔╝██║ ╚████║███████╗███████║
    '╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝╚══════╝


    'Metodo para crear pestañas junto con su textbox en el TabControl
    Private Sub nuevaPestaña()
        'Se crea un objeto tipo TabPage
        Dim tmpPestaña As New TabPage

        'Se crea un objeto tipo TextBox y se le asignan ciertas propiedades
        Dim tmpTextBox As New TextBox
        tmpTextBox.Name = "txtPestaña"
        tmpTextBox.MultiLine = True
        tmpTextBox.Location = New Point(6, 6)
        tmpTextBox.ScrollBars = ScrollBars.Vertical
        tmpTextBox.MaxLength = 10000000
        tmpTextBox.BorderStyle = BorderStyle.None
        tmpTextBox.Width = 419
        tmpTextBox.Height = 510

        'Propiedades de la pestaña
        tmpPestaña.Text = "Pestaña " + contadorPestaña.ToString
        tmpPestaña.Name = "tabP_"

        'Agregamos la Pestaña al TabControl
        TabControl_Pestañas.TabPages.Add(tmpPestaña)

        'Agregamos el textbox a la Pestaña
        tmpPestaña.Controls.Add(tmpTextBox)

        'Sumamos +1 cada vez que se crea una pestaña
        contadorPestaña += 1
    End Sub

    'Metodo para cerrar una pestaña en el TabControl
    Private Sub cerrarPestaña()
        'Se crea un Try para evitar que termine la ejecución del programa,
        'al no haber más pestañas que cerrar
        Try
            'Cierra la pestaña que esta seleccionada
            TabControl_Pestañas.TabPages.Remove(TabControl_Pestañas.SelectedTab)
        Catch ex As Exception
            MsgBox("Ooops ¿Hay pestañas?")
        End Try
    End Sub

    Private Sub abrirArchivo()
        'Se crea el objeto OpenFileDialog que nos servira para abrir un archivo
        Dim openFileDialog1 As OpenFileDialog = New OpenFileDialog

        'Filtramos las extensiones de archivo que querramos abrir
        openFileDialog1.Filter = "Archivo EST (*.py)|*.py|Todos los Archvios (*.*)|*.*"
        openFileDialog1.FilterIndex = 1

        'Evitamos que seleccionen multiples archivos a la hora de abrir, se restringe a un solo archivo
        openFileDialog1.Multiselect = False

        'Variable que nos servira para tomar ciertas acciones del OpenFileDialog (Si el usuario cancela o afirma)
        Dim UserClickedOK As Boolean = openFileDialog1.ShowDialog

        'Si el usuario confirma abrir el archivo
        If (UserClickedOK) Then
            Try
                Dim fileStream As System.IO.Stream = openFileDialog1.OpenFile
                Dim nombreArchivo = System.IO.Path.GetFileNameWithoutExtension(openFileDialog1.FileName)
                Me.TabControl_Pestañas.SelectedTab.Text = nombreArchivo
                Try
                    Using reader As New System.IO.StreamReader(fileStream)
                        Dim tmpTabPage3 As TabPage = TabControl_Pestañas.SelectedTab
                        Dim txtBox As New TextBox
                        If (tmpTabPage3.Controls.ContainsKey("txtPestaña")) Then
                            txtBox = CType(tmpTabPage3.Controls("txtPestaña"), TextBox)
                            txtBox.Text = reader.ReadToEnd
                        End If
                    End Using
                    fileStream.Close()
                Catch ex As Exception
                    MsgBox("No hay ninguna pestaña abierta, el archivo no puede ser cargado")
                End Try

            Catch ex As Exception
                Console.WriteLine(ex)
            End Try
        End If
    End Sub

    Private Sub guardarArchivo()
        Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
        SaveFileDialog1.Filter = "Archivo EST (*.py)|*.py"
        Dim tmpTabPage3 As TabPage = TabControl_Pestañas.SelectedTab
        Dim txtBox As New TextBox

        If SaveFileDialog1.ShowDialog = DialogResult.OK Then

            Try
                If (tmpTabPage3.Controls.ContainsKey("txtPestaña")) Then
                    txtBox = CType(tmpTabPage3.Controls("txtPestaña"), TextBox)
                End If
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, txtBox.Text, False)
                tmpTabPage3.Text = System.IO.Path.GetFileNameWithoutExtension(SaveFileDialog1.FileName)
            Catch ex As Exception
                MsgBox("No hay ninguna pestaña abierta, el archivo no puede ser guardado")
            End Try

        End If
    End Sub

    Private Sub abrirManualdeUsuario()
        'Obtengo la ruta en la cual se ejecuta la aplicación
        Dim rutaAplicacion As String = Path.GetDirectoryName(
            Assembly.GetExecutingAssembly().GetName().CodeBase)
        'Uno la ruta con el nombre del archivo para abrir
        Dim rutaCompleta As String = Path.Combine(rutaAplicacion, "Manual_de_Usuario_Proyecto_#2_LFP.pdf")
        'Corro el siguiente comando que inicia la abertura del pdf
        System.Diagnostics.Process.Start(rutaCompleta)
    End Sub

    Private Sub analisisLexicoSintactico()
        Dim tmpTabPage3 As TabPage = TabControl_Pestañas.SelectedTab
        Dim txtBox As New TextBox
        Try
            If (tmpTabPage3.Controls.ContainsKey("txtPestaña")) Then
                txtBox = CType(tmpTabPage3.Controls("txtPestaña"), TextBox)
                Dim Analizar As New AnalizadorLexicoSintactico(txtBox.Text, Me)
            End If
            contadorTokens = 1
            contadorErrores = 1
            MsgBox("¡Analisis terminado con exito!")
        Catch ex As Exception
            MsgBox("¡Ooops se ha encontrado un error! :(")
        End Try
    End Sub

    Private Sub acercaDe()
        MsgBox("Lenguajes Formales y Programación B- " & vbNewLine & "Mike Molina <-> 2012-12535")
    End Sub

    Private Sub crearHTML(LT As ListView, LL As ListView, LS As ListView)
        Dim HTML = New GenerarHTML(LT, LL, LS)
    End Sub

    Private Sub salirMenu()
        Dim salirOpcion As Integer = MessageBox.Show("¿Estas seguro de regresar al menu?" + vbNewLine + "Se perderan todos los datos no guardados", "Salir", MessageBoxButtons.YesNo)

        If (salirOpcion = DialogResult.Yes) Then
            'Regresa al menu principal
            Me.Hide()
            'Dim frmmenu As New frm_Principal()
            'frmmenu.Show()
            frm_Principal.Show()
        End If
    End Sub

    Private Sub cerrarFormulario(e As Object)
        If e.CloseReason = System.Windows.Forms.CloseReason.UserClosing Then
            'Cerrado desde la x del formulario o Alt + F4
            Dim salirOpcion As Integer = MessageBox.Show("¿Estas seguro que deseas regresar al menu principal?" + vbNewLine + "Se perderan todos los datos no guardados", "Salir", MessageBoxButtons.YesNo)

            If (salirOpcion = DialogResult.Yes) Then
                'Regresa al menu principal
                Me.Hide()
                'Dim frmmenu As New frm_Principal()
                'frmmenu.Show()
                frm_Principal.Show()

                'Cierra el formulario
                'e.Cancel = False

                'No hace nada
                e.Cancel = True
            Else
                'No hace nada
                e.Cancel = True
            End If
        Else
            'Cerrado por otra razón      

        End If
    End Sub

    Private Sub btn_restart_Click(sender As Object, e As EventArgs) Handles btn_restart.Click
        Application.Restart()
    End Sub

End Class