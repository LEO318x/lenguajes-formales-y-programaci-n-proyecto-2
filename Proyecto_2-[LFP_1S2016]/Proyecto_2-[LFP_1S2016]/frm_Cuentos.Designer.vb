﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_Cuentos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Cuentos))
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rtb_Cuento = New System.Windows.Forms.RichTextBox()
        Me.lbl_nombreCuento = New System.Windows.Forms.Label()
        Me.btn_Stop = New Proyecto_2__LFP_1S2016_.BonfireButton()
        Me.btn_Play = New Proyecto_2__LFP_1S2016_.BonfireButton()
        Me.lbl_titulocuento = New Proyecto_2__LFP_1S2016_.Theme.AirFoxHeader2()
        Me.AirFoxAlertBox1 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox()
        Me.AirFoxAlertBox2 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox()
        Me.AirFoxHeader21 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxHeader2()
        Me.AirFoxHeader22 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxHeader2()
        Me.AirFoxHeader23 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxHeader2()
        Me.AirFoxAlertBox3 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox()
        Me.AirFoxAlertBox4 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox()
        Me.AirFoxAlertBox5 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox()
        Me.AirFoxAlertBox6 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox()
        Me.AirFoxHeader11 = New Proyecto_2__LFP_1S2016_.Theme.AirFoxHeader1()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoScroll = True
        Me.FlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(12, 70)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(272, 469)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'rtb_Cuento
        '
        Me.rtb_Cuento.Location = New System.Drawing.Point(306, 107)
        Me.rtb_Cuento.Name = "rtb_Cuento"
        Me.rtb_Cuento.Size = New System.Drawing.Size(332, 432)
        Me.rtb_Cuento.TabIndex = 3
        Me.rtb_Cuento.Text = ""
        '
        'lbl_nombreCuento
        '
        Me.lbl_nombreCuento.AutoSize = True
        Me.lbl_nombreCuento.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nombreCuento.Location = New System.Drawing.Point(366, 71)
        Me.lbl_nombreCuento.Name = "lbl_nombreCuento"
        Me.lbl_nombreCuento.Size = New System.Drawing.Size(0, 16)
        Me.lbl_nombreCuento.TabIndex = 6
        '
        'btn_Stop
        '
        Me.btn_Stop.ButtonStyle = Proyecto_2__LFP_1S2016_.BonfireButton.Style.Dark
        Me.btn_Stop.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_Stop.Font = New System.Drawing.Font("Verdana", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.btn_Stop.Image = CType(resources.GetObject("btn_Stop.Image"), System.Drawing.Image)
        Me.btn_Stop.Location = New System.Drawing.Point(644, 160)
        Me.btn_Stop.Name = "btn_Stop"
        Me.btn_Stop.RoundedCorners = True
        Me.btn_Stop.Size = New System.Drawing.Size(66, 38)
        Me.btn_Stop.TabIndex = 8
        Me.btn_Stop.Text = "Stop"
        '
        'btn_Play
        '
        Me.btn_Play.ButtonStyle = Proyecto_2__LFP_1S2016_.BonfireButton.Style.Dark
        Me.btn_Play.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_Play.Font = New System.Drawing.Font("Verdana", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.btn_Play.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn_Play.Image = CType(resources.GetObject("btn_Play.Image"), System.Drawing.Image)
        Me.btn_Play.Location = New System.Drawing.Point(644, 116)
        Me.btn_Play.Name = "btn_Play"
        Me.btn_Play.RoundedCorners = True
        Me.btn_Play.Size = New System.Drawing.Size(66, 38)
        Me.btn_Play.TabIndex = 7
        Me.btn_Play.Text = "Play"
        '
        'lbl_titulocuento
        '
        Me.lbl_titulocuento.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lbl_titulocuento.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.lbl_titulocuento.Location = New System.Drawing.Point(306, 70)
        Me.lbl_titulocuento.Name = "lbl_titulocuento"
        Me.lbl_titulocuento.Size = New System.Drawing.Size(62, 19)
        Me.lbl_titulocuento.TabIndex = 5
        Me.lbl_titulocuento.Text = "Nombre:"
        '
        'AirFoxAlertBox1
        '
        Me.AirFoxAlertBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AirFoxAlertBox1.EnabledCalc = True
        Me.AirFoxAlertBox1.Location = New System.Drawing.Point(666, 253)
        Me.AirFoxAlertBox1.Name = "AirFoxAlertBox1"
        Me.AirFoxAlertBox1.Size = New System.Drawing.Size(53, 40)
        Me.AirFoxAlertBox1.Style = Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox.Styles.Green
        Me.AirFoxAlertBox1.TabIndex = 9
        Me.AirFoxAlertBox1.Text = "SI"
        '
        'AirFoxAlertBox2
        '
        Me.AirFoxAlertBox2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AirFoxAlertBox2.EnabledCalc = True
        Me.AirFoxAlertBox2.Location = New System.Drawing.Point(666, 253)
        Me.AirFoxAlertBox2.Name = "AirFoxAlertBox2"
        Me.AirFoxAlertBox2.Size = New System.Drawing.Size(53, 40)
        Me.AirFoxAlertBox2.Style = Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox.Styles.Red
        Me.AirFoxAlertBox2.TabIndex = 10
        Me.AirFoxAlertBox2.Text = "NO"
        '
        'AirFoxHeader21
        '
        Me.AirFoxHeader21.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.AirFoxHeader21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.AirFoxHeader21.Location = New System.Drawing.Point(643, 310)
        Me.AirFoxHeader21.Name = "AirFoxHeader21"
        Me.AirFoxHeader21.Size = New System.Drawing.Size(112, 19)
        Me.AirFoxHeader21.TabIndex = 11
        Me.AirFoxHeader21.Text = "Resaltar Palabra:"
        '
        'AirFoxHeader22
        '
        Me.AirFoxHeader22.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.AirFoxHeader22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.AirFoxHeader22.Location = New System.Drawing.Point(645, 228)
        Me.AirFoxHeader22.Name = "AirFoxHeader22"
        Me.AirFoxHeader22.Size = New System.Drawing.Size(111, 19)
        Me.AirFoxHeader22.TabIndex = 12
        Me.AirFoxHeader22.Text = "Mostrar Texto:"
        '
        'AirFoxHeader23
        '
        Me.AirFoxHeader23.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.AirFoxHeader23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.AirFoxHeader23.Location = New System.Drawing.Point(645, 392)
        Me.AirFoxHeader23.Name = "AirFoxHeader23"
        Me.AirFoxHeader23.Size = New System.Drawing.Size(111, 19)
        Me.AirFoxHeader23.TabIndex = 13
        Me.AirFoxHeader23.Text = "Hablar Texto:"
        '
        'AirFoxAlertBox3
        '
        Me.AirFoxAlertBox3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AirFoxAlertBox3.EnabledCalc = True
        Me.AirFoxAlertBox3.Location = New System.Drawing.Point(666, 335)
        Me.AirFoxAlertBox3.Name = "AirFoxAlertBox3"
        Me.AirFoxAlertBox3.Size = New System.Drawing.Size(53, 40)
        Me.AirFoxAlertBox3.Style = Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox.Styles.Green
        Me.AirFoxAlertBox3.TabIndex = 14
        Me.AirFoxAlertBox3.Text = "SI"
        '
        'AirFoxAlertBox4
        '
        Me.AirFoxAlertBox4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AirFoxAlertBox4.EnabledCalc = True
        Me.AirFoxAlertBox4.Location = New System.Drawing.Point(666, 335)
        Me.AirFoxAlertBox4.Name = "AirFoxAlertBox4"
        Me.AirFoxAlertBox4.Size = New System.Drawing.Size(53, 40)
        Me.AirFoxAlertBox4.Style = Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox.Styles.Red
        Me.AirFoxAlertBox4.TabIndex = 15
        Me.AirFoxAlertBox4.Text = "NO"
        '
        'AirFoxAlertBox5
        '
        Me.AirFoxAlertBox5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AirFoxAlertBox5.EnabledCalc = True
        Me.AirFoxAlertBox5.Location = New System.Drawing.Point(666, 417)
        Me.AirFoxAlertBox5.Name = "AirFoxAlertBox5"
        Me.AirFoxAlertBox5.Size = New System.Drawing.Size(53, 40)
        Me.AirFoxAlertBox5.Style = Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox.Styles.Green
        Me.AirFoxAlertBox5.TabIndex = 16
        Me.AirFoxAlertBox5.Text = "SI"
        '
        'AirFoxAlertBox6
        '
        Me.AirFoxAlertBox6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AirFoxAlertBox6.EnabledCalc = True
        Me.AirFoxAlertBox6.Location = New System.Drawing.Point(666, 417)
        Me.AirFoxAlertBox6.Name = "AirFoxAlertBox6"
        Me.AirFoxAlertBox6.Size = New System.Drawing.Size(53, 40)
        Me.AirFoxAlertBox6.Style = Proyecto_2__LFP_1S2016_.Theme.AirFoxAlertBox.Styles.Red
        Me.AirFoxAlertBox6.TabIndex = 17
        Me.AirFoxAlertBox6.Text = "NO"
        '
        'AirFoxHeader11
        '
        Me.AirFoxHeader11.Font = New System.Drawing.Font("Segoe UI Semibold", 20.0!)
        Me.AirFoxHeader11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.AirFoxHeader11.Location = New System.Drawing.Point(249, 12)
        Me.AirFoxHeader11.Name = "AirFoxHeader11"
        Me.AirFoxHeader11.Size = New System.Drawing.Size(330, 51)
        Me.AirFoxHeader11.TabIndex = 18
        Me.AirFoxHeader11.Text = "Bienvenido a la Biblioteca"
        '
        'frm_Cuentos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(768, 551)
        Me.Controls.Add(Me.AirFoxHeader11)
        Me.Controls.Add(Me.AirFoxAlertBox6)
        Me.Controls.Add(Me.AirFoxAlertBox5)
        Me.Controls.Add(Me.AirFoxAlertBox4)
        Me.Controls.Add(Me.AirFoxAlertBox3)
        Me.Controls.Add(Me.AirFoxHeader23)
        Me.Controls.Add(Me.AirFoxHeader22)
        Me.Controls.Add(Me.AirFoxHeader21)
        Me.Controls.Add(Me.AirFoxAlertBox2)
        Me.Controls.Add(Me.AirFoxAlertBox1)
        Me.Controls.Add(Me.btn_Stop)
        Me.Controls.Add(Me.btn_Play)
        Me.Controls.Add(Me.lbl_nombreCuento)
        Me.Controls.Add(Me.lbl_titulocuento)
        Me.Controls.Add(Me.rtb_Cuento)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_Cuentos"
        Me.Text = "Biblioteca de Cuentos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents rtb_Cuento As RichTextBox
    Friend WithEvents lbl_titulocuento As Theme.AirFoxHeader2
    Friend WithEvents lbl_nombreCuento As Label
    Friend WithEvents btn_Play As BonfireButton
    Friend WithEvents btn_Stop As BonfireButton
    Friend WithEvents AirFoxAlertBox1 As Theme.AirFoxAlertBox
    Friend WithEvents AirFoxAlertBox2 As Theme.AirFoxAlertBox
    Friend WithEvents AirFoxHeader21 As Theme.AirFoxHeader2
    Friend WithEvents AirFoxHeader22 As Theme.AirFoxHeader2
    Friend WithEvents AirFoxHeader23 As Theme.AirFoxHeader2
    Friend WithEvents AirFoxAlertBox3 As Theme.AirFoxAlertBox
    Friend WithEvents AirFoxAlertBox4 As Theme.AirFoxAlertBox
    Friend WithEvents AirFoxAlertBox5 As Theme.AirFoxAlertBox
    Friend WithEvents AirFoxAlertBox6 As Theme.AirFoxAlertBox
    Friend WithEvents AirFoxHeader11 As Theme.AirFoxHeader1
End Class
