﻿Imports System.IO
Imports System.Text.RegularExpressions
Public Class InterpretacionDatos
    Dim vTitulo = "", vCuento = "", vImagen = "", vTipoVoz = "", vVolumen = "", vVelocidad = ""
    'Dim vResaltar = "Falso", vTexto = "Falso", vTraduccion = "Falso"
    Dim vResaltar = False, vTexto = False, vTraduccion = False
    Dim vTemp() As String
    'Dim btnCuento As New Button()
    Dim cont As Integer = 0
    Dim listaPrincipal As New List(Of Dictionary(Of String, String))
    Public Sub New(Texto As String)
        Inicio(Texto)
    End Sub

    Public Sub Inicio(Texto As String)
        separarCuentos(Texto)
    End Sub

    Public Sub separarCuentos(Texto As String)
        Dim clases() As String = Split(Texto, "class")
        For i = 1 To clases.Count - 1
            extraerAtributos(clases(i), 1)
        Next
    End Sub

    Public Sub obtenerAtributos(Texto As String)

        Dim expresionER As String = ""
        Dim tipoDato As Integer = -1

        'Para lo que esta dentro del __init__
        Dim ER_Titulo As String = "(self.)?titulo\s?=\s?""[^""]+""|(self.)?titulo\s?=\s?\w+"
        Dim ER_Cuento As String = "(self.)?cuento\s?=\s?""[^""]+""|(self.)?cuento\s?=\s?\w+"
        Dim ER_Imagen As String = "(self.)?imagen\s?=\s?""[^""]+""|(self.)?imagen\s?=\s?\w+"
        Dim ER_Voz As String = "(self.)?tipoVoz\s?=\s?""[^""]+""|(self.)?tipoVoz\s?=\s?\w+"
        Dim ER_Volumen As String = "(self.)?volumen\s?=\s?(\d+|\*|\+|\-|\/|\(|\))+|(self.)?volumen\s?=\s?\w+"
        Dim ER_Velocidad As String = "(self.)?velocidad\s?=\s?(\d+|\*|\+|\-|\/|\(|\))+|(self.)?velocidad\s?=\s?\w+"

        'Para los otros parametros
        Dim ER_Resaltar As String = "(self.)?resaltar\s?=\s?(True|False)|(self.)?resaltar\s?=\s?\w+"
        Dim ER_Texto As String = "(self.)?texto\s?=\s?(True|False)|(self.)?texto\s?=\s?\w+"
        Dim ER_Traduccion As String = "(self.)?traduccion\s?=\s?(True|False)|(self.)?traduccion\s?=\s?\w+"

        For i = 0 To 8
            Select Case i
                Case 0
                    expresionER = ER_Titulo
                    tipoDato = 0
                Case 1
                    expresionER = ER_Cuento
                    tipoDato = 1
                Case 2
                    expresionER = ER_Imagen
                    tipoDato = 2
                Case 3
                    expresionER = ER_Voz
                    tipoDato = 3
                Case 4
                    expresionER = ER_Volumen
                    tipoDato = 4
                Case 5
                    expresionER = ER_Velocidad
                    tipoDato = 5
                Case 6
                    expresionER = ER_Resaltar
                    tipoDato = 6
                Case 7
                    expresionER = ER_Texto
                    tipoDato = 7
                Case 8
                    expresionER = ER_Traduccion
                    tipoDato = 8
            End Select
            Dim regex As New Regex(expresionER)
            For Each match As Match In regex.Matches(Texto)
                If match.Success Then
                    'Console.WriteLine(match.Value)
                    extraerDatos(match.Value, tipoDato)
                End If
            Next
        Next

        agregarCuento()

    End Sub

    Private Sub extraerDatos(Parametro As String, tipoDato As Integer)

        Select Case tipoDato
            Case 0
                vTemp = Split(Parametro, "=")
                'Console.WriteLine(Trim(vTemp(1)))
                vTitulo = Trim(vTemp(1))
            Case 1
                vTemp = Split(Parametro, "=")
                'Console.WriteLine(Trim(vTemp(1)))
                vCuento = Trim(vTemp(1))
            Case 2
                vTemp = Split(Parametro, "=")
                'Console.WriteLine(Trim(vTemp(1)))
                vImagen = Trim(vTemp(1))
            Case 3
                vTemp = Split(Parametro, "=")
                'Console.WriteLine(Trim(vTemp(1)))
                vTipoVoz = Trim(vTemp(1))
            Case 4
                vTemp = Split(Parametro, "=")
                'Console.WriteLine(realizarOperacion(Trim(vTemp(1))))
                vVolumen = realizarOperacion(Trim(vTemp(1)))
            Case 5
                vTemp = Split(Parametro, "=")
                'Console.WriteLine(realizarOperacion(Trim(vTemp(1))))
                vVelocidad = realizarOperacion(Trim(vTemp(1)))
            Case 6
                vTemp = Split(Parametro, "=")
                vResaltar = Trim(vTemp(1))

                'Console.WriteLine(Trim(vTemp(1)))
            Case 7
                vTemp = Split(Parametro, "=")
                vTexto = Trim(vTemp(1))
            Case 8
                vTemp = Split(Parametro, "=")
                vTraduccion = Trim(vTemp(1))
                'Console.WriteLine(Trim(vTemp(1)))
        End Select

    End Sub

    Public Sub extraerAtributos(Texto As String, tipoExtraccion As Integer)

        If (tipoExtraccion = 1) Then

            Dim lista As New List(Of String)
            'Console.WriteLine(Texto)
            'Dim expresionER As String = ""


            'Obtener atributos que se encuentran en una sola linea
            Dim ER_VariosAtributos = "([a-zA-Z]+|[a-zA-Z]([a-zA-Z]|\d)+)(\s*,\s*([a-zA-Z]+|[a-zA-Z]([a-zA-Z]|\d)+))+\s*=\s*(""[^""]+""|(\(|\)|\+|\-|\*|\/|[0-9])+)(\s*,\s*(""[^""]+""|(\(|\)|\+|\-|\*|\/|[0-9])+))*"
            Dim expresionER As String = ER_VariosAtributos
            'Parametros de la expresion regular
            Dim opcionesER As RegexOptions = RegexOptions.None


            Dim regex As New Regex(expresionER, opcionesER)
            For Each match As Match In regex.Matches(Texto)
                If match.Success Then
                    'Console.WriteLine(match.Value)
                    lista.Add(match.Value)
                    'testReemplazo(match.Value, Texto)
                    'extraerDatos(match.Value, tipoDato)
                End If
            Next
            ReemplazoAtributosLinea(lista, Texto)

        ElseIf (tipoExtraccion = 2) Then
            Dim lista As New List(Of String)
            'Console.WriteLine(Texto)
            'Dim expresionER As String = ""


            'Obtener atributos que se encuentran en una sola linea
            Dim ER_VariosAtributos = "\s([a-zA-Z]+|[a-zA-Z]([a-zA-Z]|\d)+)+\s*=\s*(""[^""]+""|(\(|\)|\+|\-|\*|\/|[0-9])+)"
            Dim expresionER As String = ER_VariosAtributos
            'Parametros de la expresion regular
            Dim opcionesER As RegexOptions = RegexOptions.None


            Dim regex As New Regex(expresionER, opcionesER)
            For Each match As Match In regex.Matches(Texto)
                If match.Success Then
                    'Console.WriteLine((match.Value).Trim)
                    lista.Add(match.Value)
                    'testReemplazo(match.Value, Texto)
                    'extraerDatos(match.Value, tipoDato)
                End If
            Next
            ReemplazoAtributosIndividual(lista, Texto)
        End If
    End Sub



    Public Sub ReemplazoAtributosIndividual(listaAtributos As List(Of String), texto As String)
        For i3 = 0 To listaAtributos.Count - 1
            Dim tmpAtributos = listaAtributos.Item(i3)
            texto = Replace(texto, tmpAtributos, "")
            Dim tmpTexto As String = texto
            Dim dividirAtributos() = Split(tmpAtributos, "=")
            Dim tmpAtributosIZQ = dividirAtributos(0).Trim
            Dim tmpAtributosDER = dividirAtributos(1).Trim
            Dim contarAtributosIZQ = tmpAtributosIZQ.Count
            Dim contarAtributosDER = tmpAtributosDER.Count
            'Console.WriteLine(tmpAtributosIZQ & "->" & tmpAtributosDER)
            texto = Regex.Replace(texto, "= " & Trim(tmpAtributosIZQ), "= " & Trim(tmpAtributosDER))
        Next
        obtenerAtributos(texto)
        'Console.WriteLine(texto)
    End Sub

    Public Sub ReemplazoAtributosLinea(listaAtributos As List(Of String), texto As String)
        For i2 = 0 To listaAtributos.Count - 1
            Dim tmpAtributos = listaAtributos.Item(i2)
            texto = Replace(texto, tmpAtributos, "")
            'Console.WriteLine(texto)
            Dim tmpTexto As String = texto
            Dim dividirAtributos() = Split(tmpAtributos, "=")
            Dim tmpAtributosIZQ() = Split(dividirAtributos(0), ",")
            Dim tmpAtributosDER() = Split(dividirAtributos(1), ",")
            Dim contarAtributosIZQ = tmpAtributosIZQ.Count
            Dim contarAtributosDER = tmpAtributosDER.Count
            If (contarAtributosIZQ = contarAtributosDER) Then
                For i = 0 To contarAtributosIZQ - 1
                    'Console.WriteLine(Trim(tmpAtributosIZQ(i)) & "->" & Trim(tmpAtributosDER(i)))
                    texto = Regex.Replace(texto, "= " & Trim(tmpAtributosIZQ(i)), "= " & Trim(tmpAtributosDER(i)))
                Next
                'Console.WriteLine(texto)
            Else
                MsgBox("Faltan atributos")
            End If
        Next
        extraerAtributos(texto, 2)
    End Sub

    Private Function realizarOperacion(Ecuacion As String)
        Dim sc As New MSScriptControl.ScriptControl()
        sc.Language = "VBScript"
        Dim resultado As Double = sc.Eval(Ecuacion)
        Return resultado
    End Function

    Public Sub agregarCuento()

        Dim btnCuento As New Button()
        Dim Dict As New Dictionary(Of String, String)
        Dict.Add("titulo", vTitulo)
        Dict.Add("textocuento", vCuento)
        Dict.Add("imagen", vImagen)
        Dict.Add("tipovoz", vTipoVoz)
        Dict.Add("volumen", vVolumen)
        Dict.Add("velocidad", vVelocidad)
        Dict.Add("traduccion", vTraduccion)
        Dict.Add("texto", vTexto)
        Dict.Add("resaltar", vResaltar)


        listaPrincipal.Add(Dict)
        btnCuento.Width = 128
        btnCuento.Height = 128
        btnCuento.Text = Replace(vTitulo, """", "")
        btnCuento.Name = cont
        btnCuento.TextAlign = ContentAlignment.BottomCenter
        If (File.Exists(Replace(vImagen, """", ""))) Then
            btnCuento.BackgroundImage = System.Drawing.Image.FromFile(Replace(vImagen, """", ""))
            btnCuento.BackgroundImageLayout = ImageLayout.Stretch
        Else
            btnCuento.BackgroundImage = My.Resources.No_Image_Available
            btnCuento.BackgroundImageLayout = ImageLayout.Stretch
        End If
        AddHandler btnCuento.Click, AddressOf opcionesBotonesCuentos
        frm_Cuentos.FlowLayoutPanel1.Controls.Add(btnCuento)
        cont += 1

    End Sub

    Private Sub opcionesBotonesCuentos(sender As Object, e As EventArgs)
        frm_Cuentos.pararReproduccion()
        frm_Cuentos.rtb_Cuento.DeselectAll()
        frm_Cuentos.rtb_Cuento.Text = ""
        frm_Cuentos.rtb_Cuento.SelectionColor = Color.Black
        Dim nombre = CType(sender, Button).Name
        'Console.WriteLine(nombre)
        Dim contenidoLista = CType(listaPrincipal.Item(nombre), Dictionary(Of String, String))
        frm_Cuentos.lbl_nombreCuento.Text = contenidoLista.Item("titulo")


        frm_Cuentos.met_textoCuento(Replace(contenidoLista.Item("textocuento"), """", ""))


        If (contenidoLista.Item("traduccion")) Then
            frm_Cuentos.met_leerTexto(1)
        Else
            frm_Cuentos.met_leerTexto(2)
        End If


        If (contenidoLista.Item("texto")) Then
            frm_Cuentos.met_mostrarTexto(1)
            frm_Cuentos.agregarTextoCuento(Replace(contenidoLista.Item("textocuento"), """", ""))
        Else
            frm_Cuentos.met_mostrarTexto(2)
        End If

        If (contenidoLista.Item("resaltar")) Then
            frm_Cuentos.met_resaltarTexto(1)
        Else
            frm_Cuentos.met_resaltarTexto(2)
        End If

        If (CType(contenidoLista.Item("volumen"), Integer) > 100) Then
            MsgBox("El volumen de este cuento es de: " & contenidoLista.Item("volumen") & "." & vbNewLine & "Supera el limite maximo permitido por lo tanto se ha establecido a 100")
            frm_Cuentos.modificarVolumen("100")
        ElseIf (CType(contenidoLista.Item("volumen"), Integer) < 0) Then
            MsgBox("El volumen de este cuento es de: " & contenidoLista.Item("volumen") & "." & vbNewLine & "Supera el limite minimo permitido por lo tanto se ha establecido a 0")
            frm_Cuentos.modificarVolumen("0")
        Else
            frm_Cuentos.modificarVolumen(contenidoLista.Item("volumen"))
        End If

        If (CType(contenidoLista.Item("velocidad"), Integer) > 10) Then
            MsgBox("La velocidad de este cuento es de: " & contenidoLista.Item("velocidad") & "." & vbNewLine & "Supera el limite maximo permitido por lo tanto se ha establecido a 10")
            frm_Cuentos.modificarVelocidad("10")
        ElseIf (CType(contenidoLista.Item("velocidad"), Integer) < -10) Then
            MsgBox("La velocidad de este cuento es de: " & contenidoLista.Item("velocidad") & "." & vbNewLine & "Supera el limite minimo permitido por lo tanto se ha establecido a -10")
            frm_Cuentos.modificarVelocidad("-10")
        Else
            frm_Cuentos.modificarVelocidad(contenidoLista.Item("velocidad"))
        End If
        Console.WriteLine(contenidoLista.Item("tipovoz").ToLower)
        If (contenidoLista.Item("tipovoz").ToLower = """female""") Then
            frm_Cuentos.modificarTipoVoz("1")
        ElseIf (contenidoLista.Item("tipovoz").ToLower = """male""") Then
            frm_Cuentos.modificarTipoVoz("2")
        End If

        'Console.WriteLine(contenidoLista.Item("traduccion"))
        'Console.WriteLine(contenidoLista.Item("texto"))
        'Console.WriteLine(contenidoLista.Item("resaltar"))


        'If (Replace(contenidoLista.Item("texto"), """", "") = True) Then
        '    frm_Cuentos.met_mostrarTexto(1)
        '    'frm_Cuentos.agregarTextoCuento(Replace(contenidoLista.Item("textocuento"), """", ""))
        '    '
        'Else
        '    frm_Cuentos.met_mostrarTexto(0)
        'End If

        'If (Replace(contenidoLista.Item("traduccion"), """", "") = "True") Then
        '    frm_Cuentos.met_leerTexto(1)
        'Else
        '    frm_Cuentos.met_leerTexto(0)
        'End If

        'If (Replace(contenidoLista.Item("resaltar"), """", "") = "True") Then
        '    frm_Cuentos.met_resaltarTexto(1)
        'Else
        '    frm_Cuentos.met_resaltarTexto(0)
        'End If
        'frm_Cuentos.Refresh()
        'Console.WriteLine("-> " & Replace(contenidoLista.Item("texto"), """", ""))
    End Sub
End Class
