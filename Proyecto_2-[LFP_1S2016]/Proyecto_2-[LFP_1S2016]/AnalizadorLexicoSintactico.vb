﻿Imports System.Text.RegularExpressions

Public Class AnalizadorLexicoSintactico
    Dim car() As Char
    Dim estado As Integer = 0
    Dim contadorFilas As Integer = 1
    Dim contadorColumnas As Integer = 1
    Dim contadorT = 1, contadorErroresL = 1, contadorErroresS = 1
    Dim rPalabras = False
    Dim tmpToken As String = ""
    Dim FormObjs As Object


    Public Sub New(txtBox As String, Formulario As Object)
        FormObjs = DirectCast(Formulario, Form)
        frm_Desarrollador.BonfireAlertBox1.Visible = False
        frm_Desarrollador.BonfireAlertBox2.Visible = False
        func_limpiarToken()
        Inicio(txtBox)
    End Sub

    Private Sub Inicio(texto As String)
        texto = texto.Replace(vbCrLf, vbLf)
        car = texto.ToCharArray
        analisisLexico()
        analisisSintactico()
        If (frm_Desarrollador.ListViewErroresLexicos.Items.Count = 0 And frm_Desarrollador.ListViewErroresSintacticos.Items.Count = 0) Then
            frm_Desarrollador.BonfireAlertBox2.Visible = True
            AnalisisDatos(texto)
        Else
            frm_Desarrollador.BonfireAlertBox2.Visible = False
            frm_Desarrollador.BonfireAlertBox1.Visible = True
        End If

        'Console.WriteLine(texto)
    End Sub

    '██╗     ███████╗██╗  ██╗██╗ ██████╗ ██████╗ 
    '██║     ██╔════╝╚██╗██╔╝██║██╔════╝██╔═══██╗
    '██║     █████╗   ╚███╔╝ ██║██║     ██║   ██║
    '██║     ██╔══╝   ██╔██╗ ██║██║     ██║   ██║
    '███████╗███████╗██╔╝ ██╗██║╚██████╗╚██████╔╝
    '╚══════╝╚══════╝╚═╝  ╚═╝╚═╝ ╚═════╝ ╚═════╝
    Private Sub analisisLexico()
        For pos = 0 To car.Count - 1
            Select Case estado

                ' ██████╗ 
                '██╔═████╗
                '██║██╔██║
                '████╔╝██║
                '╚██████╔╝
                ' ╚═════╝ 
                Case 0
                    'Console.WriteLine("x0")
                    If (car(pos) = " " Or car(pos) = vbTab) Then
                        estado = 0
                    ElseIf (car(pos) = vbLf) Then
                        'Console.WriteLine("Entra enter")
                        estado = 0
                        contadorFilas += 1
                        contadorColumnas = 0
                    ElseIf (Char.IsLetter(car(pos))) Then
                        'tmpToken += car(pos)
                        estado = 1
                        pos -= 1
                        contadorColumnas -= 1
                    ElseIf (car(pos) = """") Then
                        If (pos = car.Count - 1) Then
                            estado = -1
                            pos -= 1
                            contadorColumnas -= 1
                        Else
                            estado = 6
                            tmpToken += car(pos)
                        End If
                    ElseIf (car(pos) = "#") Then
                        tmpToken += car(pos)
                        estado = 15
                    ElseIf (car(pos) = "_") Then
                        tmpToken += car(pos)
                        estado = 17
                    ElseIf (car(pos) = "(" Or car(pos) = ")" Or car(pos) = "{" Or car(pos) = "}" Or car(pos) = "=" Or car(pos) = "," Or car(pos) = "." Or car(pos) = "-" Or car(pos) = "+" Or car(pos) = "*" Or car(pos) = "/") Then
                        'Console.WriteLine("### " & tmpToken.Count & " " & contadorColumnas)
                        estado = 22
                        pos -= 1
                        contadorColumnas -= 1
                    ElseIf (Char.IsDigit(car(pos))) Then
                        estado = 23
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        'error
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    End If

                ' ██╗
                '███║
                '╚██║
                ' ██║
                ' ██║
                ' ╚═╝
                Case 1
                    'Console.WriteLine("x1")
                    If (Char.IsLetterOrDigit(car(pos))) Then
                        If (pos = car.Count - 1) Then
                            tmpToken += car(pos)
                            If (revisarToken(tmpToken)) Then
                                tmpToken = ""
                                estado = 0
                            Else
                                'func_AgregarToken(contadorFilas, (contadorColumnas - tmpToken.Length), tmpToken, "1", "VAR/CLASE")
                                'xError(tmpToken)
                                'estado = 0
                            End If
                        Else
                            tmpToken += car(pos)
                            estado = 1
                        End If
                    ElseIf (car(pos) = "_") Then
                        tmpToken += car(pos)
                        estado = 2
                    Else
                        If (revisarToken(tmpToken)) Then
                            tmpToken = ""
                        Else
                            'xError(tmpToken)
                            'estado = 0
                        End If
                        tmpToken = ""
                        estado = 0
                        pos -= 1
                        contadorColumnas -= 1
                    End If


                '██████╗ 
                '╚════██╗
                ' █████╔╝
                '██╔═══╝ 
                '███████╗
                '╚══════╝
                Case 2
                    'Console.WriteLine("x2")
                    If (Char.IsLetterOrDigit(car(pos)) And pos = car.Count - 1) Then
                        estado = 3
                        'estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        If (Char.IsLetterOrDigit(car(pos))) Then
                            'tmpToken += car(pos)
                            estado = 3
                            pos -= 1
                            contadorColumnas -= 1
                        Else
                            estado = -1
                            pos -= 1
                            contadorColumnas -= 1
                        End If
                    End If

                '██████╗ 
                '╚════██╗
                ' █████╔╝
                ' ╚═══██╗
                '██████╔╝
                '╚═════╝ 
                Case 3
                    'Console.WriteLine("x3")
                    ''If (Char.IsLetterOrDigit(car(pos)) And pos = car.Count - 1) Then
                    ''    tmpToken += car(pos)
                    ''    If (revisarToken(tmpToken)) Then
                    ''        'Console.WriteLine("Ok - " & tmpToken)
                    ''        tmpToken = ""
                    ''        estado = 0
                    ''    Else
                    ''        tmpToken = ""
                    ''        estado = 0
                    ''    End If
                    ''ElseIf (Char.IsLetterOrDigit(car(pos))) Then
                    ''    estado = 3
                    ''    tmpToken += car(pos)
                    ''ElseIf (car(pos) = " " Or car(pos) = vbTab Or car(pos) = vbLf) Then
                    ''    If (revisarToken(tmpToken)) Then
                    ''        'Console.WriteLine("Ok - " & tmpToken)
                    ''        tmpToken = ""
                    ''        estado = 0
                    ''    Else
                    ''        tmpToken = ""
                    ''        estado = 0
                    ''    End If
                    ''ElseIf (car(pos) = "_") Then
                    ''    If (pos = car.Count - 1) Then
                    ''        estado = -1
                    ''        pos -= 1
                    ''        contadorColumnas -= 1
                    ''    Else
                    ''        estado = 4
                    ''        tmpToken += car(pos)
                    ''        'pos -= 1
                    ''        'contadorColumnas -= 1
                    ''    End If
                    ''Else
                    ''    If (revisarToken(tmpToken)) Then
                    ''        'Console.WriteLine("Ok - " & tmpToken)
                    ''        tmpToken = ""
                    ''        estado = 0
                    ''        pos -= 1
                    ''        contadorColumnas -= 1
                    ''    Else
                    ''        tmpToken = ""
                    ''        estado = -1
                    ''        pos -= 1
                    ''        contadorColumnas -= 1
                    ''    End If
                    ''End If
                    If (Char.IsLetterOrDigit(car(pos))) Then
                        If (pos = car.Count - 1) Then
                            tmpToken += car(pos)
                            If (revisarToken(tmpToken)) Then
                                tmpToken = ""
                                estado = 0
                            Else
                                'xError(tmpToken)
                                'estado = 0
                            End If
                        Else
                            tmpToken += car(pos)
                            estado = 3
                        End If
                    ElseIf (car(pos) = "_") Then
                        tmpToken += car(pos)
                        estado = 4
                    Else
                        If (revisarToken(tmpToken)) Then
                            tmpToken = ""
                        Else
                            'xError(tmpToken)
                            'estado = 0
                        End If
                        tmpToken = ""
                        estado = 0
                        pos -= 1
                        contadorColumnas -= 1
                    End If

                '██╗  ██╗
                '██║  ██║
                '███████║
                '╚════██║
                '     ██║
                '     ╚═╝
                Case 4
                    'Console.WriteLine("x4")
                    If (Char.IsLetterOrDigit(car(pos)) And pos = car.Count - 1) Then
                        estado = 5
                        'estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        If (Char.IsLetterOrDigit(car(pos))) Then
                            'tmpToken += car(pos)
                            estado = 5
                            pos -= 1
                            contadorColumnas -= 1
                        Else
                            estado = -1
                            pos -= 1
                            contadorColumnas -= 1
                        End If
                    End If


                '███████╗
                '██╔════╝
                '███████╗
                '╚════██║
                '███████║
                '╚══════╝
                Case 5
                    'Console.WriteLine("x5")
                    'If (Char.IsLetterOrDigit(car(pos)) And pos = car.Count - 1) Then
                    '    tmpToken += car(pos)
                    '    If (revisarToken(tmpToken)) Then
                    '        'Console.WriteLine("Ok - " & tmpToken)
                    '        tmpToken = ""
                    '        estado = 0
                    '    Else
                    '        tmpToken = ""
                    '        estado = 0
                    '    End If
                    'ElseIf (Char.IsLetterOrDigit(car(pos))) Then
                    '    estado = 5
                    '    tmpToken += car(pos)
                    'ElseIf (car(pos) = " " Or car(pos) = vbTab Or car(pos) = vbLf) Then
                    '    If (revisarToken(tmpToken)) Then
                    '        'Console.WriteLine("Ok - " & tmpToken)
                    '        tmpToken = ""
                    '        estado = 0
                    '    Else
                    '        tmpToken = ""
                    '        estado = 0
                    '    End If
                    'ElseIf (car(pos) = "_" And pos = car.Count - 1) Then
                    '    estado = -1
                    '    pos -= 1
                    '    contadorColumnas -= 1
                    'Else
                    '    If (car(pos) = "_") Then
                    '        tmpToken += car(pos)
                    '        estado = 4
                    '    ElseIf (revisarToken(tmpToken)) Then
                    '        'Console.WriteLine("Ok - " & tmpToken)
                    '        tmpToken = ""
                    '        estado = 0
                    '        pos -= 1
                    '        contadorColumnas -= 1
                    '    Else
                    '        tmpToken = ""
                    '        estado = -1
                    '        pos -= 1
                    '        contadorColumnas -= 1
                    '    End If
                    'End If
                    If (Char.IsLetterOrDigit(car(pos))) Then
                        If (pos = car.Count - 1) Then
                            tmpToken += car(pos)
                            If (revisarToken(tmpToken)) Then
                                tmpToken = ""
                                estado = 0
                            Else
                                'xError(tmpToken)
                                'estado = 0
                            End If
                        Else
                            tmpToken += car(pos)
                            estado = 5
                        End If
                    ElseIf (car(pos) = "_") Then
                        tmpToken += car(pos)
                        estado = 4
                    Else
                        If (revisarToken(tmpToken)) Then
                            tmpToken = ""
                        Else
                            'xError(tmpToken)
                            'estado = 0
                        End If
                        tmpToken = ""
                        estado = 0
                        pos -= 1
                        contadorColumnas -= 1
                    End If


                ' ██████╗ 
                '██╔════╝ 
                '███████╗ 
                '██╔═══██╗
                '╚██████╔╝
                ' ╚═════╝ 
                Case 6
                    'Console.WriteLine("x6")
                    'If (pos = car.Count - 1 Or car(pos) = vbLf) Then
                    If (pos = car.Count - 1) Then
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        If (car(pos) = """") Then
                            tmpToken += car(pos)
                            estado = 9
                            'pos -= 1
                            'contadorColumnas -= 1
                        Else
                            estado = 7
                            tmpToken += car(pos)
                        End If
                    End If

                '███████╗
                '╚════██║
                '    ██╔╝
                '   ██╔╝ 
                '   ██║  
                '   ╚═╝  
                Case 7
                    'Console.WriteLine("x7")
                    If (pos = car.Count - 1) Then
                        If (car(pos) = """") Then
                            estado = 8
                            pos -= 1
                            contadorColumnas -= 1
                        Else
                            estado = -1
                            pos -= 1
                            contadorColumnas -= 1
                        End If
                    Else
                        If (car(pos) = """") Then
                            estado = 8
                            pos -= 1
                            contadorColumnas -= 1
                            'ElseIf (car(pos) = vbLf) Then
                            '    estado = -1
                            '    pos -= 1
                            '    contadorColumnas -= 1
                        Else
                            tmpToken += car(pos)
                            estado = 7
                        End If
                    End If

                ' █████╗ 
                '██╔══██╗
                '╚█████╔╝
                '██╔══██╗
                '╚█████╔╝
                ' ╚════╝ 
                Case 8
                    'Console.WriteLine("x8")
                    tmpToken += car(pos)
                    If (revisarToken(tmpToken)) Then
                        'Console.WriteLine("ok - " & tmpToken)
                        tmpToken = ""
                        estado = 0
                    Else
                        tmpToken = ""
                        estado = 0
                    End If

                ' █████╗ 
                '██╔══██╗
                '╚██████║
                ' ╚═══██║
                ' █████╔╝
                ' ╚════╝ 
                Case 9
                    'Console.WriteLine("x9")
                    If (pos = car.Count - 1 Or car(pos) = vbLf) Then
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        If (car(pos) = """") Then
                            tmpToken += car(pos)
                            estado = 10
                            'pos -= 1
                            'contadorColumnas -= 1
                        Else
                            estado = -1
                            pos -= 1
                            contadorColumnas -= 1
                        End If
                    End If


                ' ██╗ ██████╗ 
                '███║██╔═████╗
                '╚██║██║██╔██║
                ' ██║████╔╝██║
                ' ██║╚██████╔╝
                ' ╚═╝ ╚═════╝ 
                Case 10
                    'Console.WriteLine("x10")
                    If (pos = car.Count - 1) Then
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        tmpToken += car(pos)
                        estado = 11
                    End If

                ' ██╗ ██╗
                '███║███║
                '╚██║╚██║
                ' ██║ ██║
                ' ██║ ██║
                ' ╚═╝ ╚═╝
                Case 11
                    'Console.WriteLine("x11")
                    If (pos = car.Count - 1) Then
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        If (car(pos) = """") Then
                            tmpToken += car(pos)
                            estado = 12
                            'pos -= 1
                            'contadorColumnas -= 1
                        Else
                            tmpToken += car(pos)
                            estado = 11
                        End If
                    End If

                ' ██╗██████╗ 
                '███║╚════██╗
                '╚██║ █████╔╝
                ' ██║██╔═══╝ 
                ' ██║███████╗
                ' ╚═╝╚══════╝
                Case 12
                    'Console.WriteLine("x12")
                    If (pos = car.Count - 1 Or car(pos) = vbLf) Then
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        If (car(pos) = """") Then
                            tmpToken += car(pos)
                            estado = 13
                            'pos -= 1
                            'contadorColumnas -= 1
                        Else
                            estado = -1
                            pos -= 1
                            contadorColumnas -= 1
                        End If
                    End If

                ' ██╗██████╗ 
                '███║╚════██╗
                '╚██║ █████╔╝
                ' ██║ ╚═══██╗
                ' ██║██████╔╝
                ' ╚═╝╚═════╝ 
                Case 13
                    'Console.WriteLine("x13")
                    If (pos = car.Count - 1 Or car(pos) = vbLf) Then
                        If (car(pos) = """") Then
                            tmpToken += car(pos)
                            estado = 14
                            pos -= 1
                            contadorColumnas -= 1
                        Else
                            estado = -1
                            pos -= 1
                            contadorColumnas -= 1
                        End If
                    Else
                        If (car(pos) = """") Then
                            tmpToken += car(pos)
                            estado = 14
                            pos -= 1
                            contadorColumnas -= 1
                        Else
                            estado = -1
                            pos -= 1
                            contadorColumnas -= 1
                        End If
                    End If

                ' ██╗██╗  ██╗
                '███║██║  ██║
                '╚██║███████║
                ' ██║╚════██║
                ' ██║     ██║
                ' ╚═╝     ╚═╝
                Case 14
                    'Console.WriteLine("x14")
                    'tmpToken += car(pos)
                    If (revisarToken(tmpToken)) Then
                        'Console.WriteLine("ok - " & tmpToken)
                        tmpToken = ""
                        estado = 0
                    Else
                        tmpToken = ""
                        estado = 0
                    End If

                ' ██╗███████╗
                '███║██╔════╝
                '╚██║███████╗
                ' ██║╚════██║
                ' ██║███████║
                ' ╚═╝╚══════╝
                Case 15
                    'Console.WriteLine("x15")
                    If (pos = car.Count - 1 Or car(pos) = vbLf) Then
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        tmpToken += car(pos)
                        estado = 16
                    End If

                ' ██╗ ██████╗ 
                '███║██╔════╝ 
                '╚██║███████╗ 
                ' ██║██╔═══██╗
                ' ██║╚██████╔╝
                ' ╚═╝ ╚═════╝
                Case 16
                    'Console.WriteLine("x16")
                    If (pos = car.Count - 1 Or car(pos) = vbLf) Then
                        If (revisarToken(tmpToken)) Then
                            'Console.WriteLine("ok - " & tmpToken)
                            tmpToken = ""
                            estado = 0
                        Else
                            tmpToken = ""
                            estado = 0
                        End If
                    Else
                        tmpToken += car(pos)
                        estado = 16
                    End If

                ' ██╗███████╗
                '███║╚════██║
                '╚██║    ██╔╝
                ' ██║   ██╔╝ 
                ' ██║   ██║  
                ' ╚═╝   ╚═╝ 
                Case 17
                    'Console.WriteLine("x17")
                    If (pos = car.Count - 1 Or car(pos) = vbLf) Then
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    ElseIf (car(pos) = "_") Then
                        tmpToken += car(pos)
                        estado = 18
                    Else
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    End If

                ' ██╗ █████╗ 
                '███║██╔══██╗
                '╚██║╚█████╔╝
                ' ██║██╔══██╗
                ' ██║╚█████╔╝
                ' ╚═╝ ╚════╝ 
                Case 18
                    'Console.WriteLine("x18")
                    If (pos = car.Count - 1 Or car(pos) = vbLf) Then
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    ElseIf (Char.IsLetter(car(pos))) Then
                        tmpToken += car(pos)
                        estado = 19
                    Else
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    End If

                ' ██╗ █████╗ 
                '███║██╔══██╗
                '╚██║╚██████║
                ' ██║ ╚═══██║
                ' ██║ █████╔╝
                ' ╚═╝ ╚════╝ 
                Case 19
                    'Console.WriteLine("x19")
                    If (pos = car.Count - 1 Or car(pos) = vbLf) Then
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    ElseIf (Char.IsLetter(car(pos))) Then
                        tmpToken += car(pos)
                        estado = 19
                    ElseIf (car(pos) = "_") Then
                        tmpToken += car(pos)
                        estado = 20
                    Else
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    End If

                '██████╗  ██████╗ 
                '╚════██╗██╔═████╗
                ' █████╔╝██║██╔██║
                '██╔═══╝ ████╔╝██║
                '███████╗╚██████╔╝
                '╚══════╝ ╚═════╝ 
                Case 20
                    'Console.WriteLine("x20")
                    If (car(pos) = "_") Then
                        tmpToken += car(pos)
                        estado = 21
                        pos -= 1
                        contadorColumnas -= 1
                    ElseIf (pos = car.Count - 1 Or car(pos) = vbLf) Then
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        estado = -1
                        pos -= 1
                        contadorColumnas -= 1
                    End If

                '██████╗  ██╗
                '╚════██╗███║
                ' █████╔╝╚██║
                '██╔═══╝  ██║
                '███████╗ ██║
                '╚══════╝ ╚═╝
                Case 21
                    'Console.WriteLine("x21")
                    If (revisarToken(tmpToken)) Then
                        'Console.WriteLine("ok - " & tmpToken)
                        tmpToken = ""
                        estado = 0
                    Else
                        tmpToken = ""
                        estado = 0
                    End If


                '██████╗ ██████╗ 
                '╚════██╗╚════██╗
                ' █████╔╝ █████╔╝
                '██╔═══╝ ██╔═══╝ 
                '███████╗███████╗
                '╚══════╝╚══════╝
                Case 22
                    'Console.WriteLine("x22")
                    tmpToken = car(pos)
                    If (revisarToken(tmpToken)) Then
                        'Console.WriteLine("Ok - " & tmpToken)
                        tmpToken = ""
                        estado = 0
                    Else
                        tmpToken = ""
                        estado = 0
                    End If

                '██████╗ ██████╗ 
                '╚════██╗╚════██╗
                ' █████╔╝ █████╔╝
                '██╔═══╝  ╚═══██╗
                '███████╗██████╔╝
                '╚══════╝╚═════╝ 
                Case 23
                    'Console.WriteLine("x23")
                    ''If (car(pos) = " " Or car(pos) = vbTab) Then
                    ''    estado = 23
                    ''    'contadorColumnas += 1
                    ''    'Console.WriteLine("Espacio/Tabulación")
                    ''ElseIf (car(pos) = vbLf) Then
                    ''    estado = 23
                    ''    'Console.WriteLine("Salto de linea")
                    ''    contadorFilas += 1
                    ''    contadorColumnas = 0
                    ''ElseIf (Char.IsDigit(car(pos)) And pos = car.Count - 1) Then
                    ''    tmpToken += car(pos)
                    ''    If (revisarToken(tmpToken)) Then
                    ''        'Console.WriteLine("Ok - " & tmpToken)
                    ''        tmpToken = ""
                    ''        estado = 0
                    ''    Else
                    ''        tmpToken = ""
                    ''        estado = 0
                    ''    End If
                    ''ElseIf (Char.IsDigit(car(pos))) Then
                    ''    estado = 23
                    ''    tmpToken += car(pos)
                    ''    'ElseIf (car(pos) = " " Or car(pos) = vbTab Or car(pos) = vbLf) Then
                    ''    '    If (revisarToken(tmpToken)) Then
                    ''    '        'Console.WriteLine("Ok - " & tmpToken)
                    ''    '        tmpToken = ""
                    ''    '        estado = 0
                    ''    '    Else
                    ''    '        tmpToken = ""
                    ''    '        estado = 0
                    ''    '    End If
                    ''Else
                    ''    If (revisarToken(tmpToken)) Then
                    ''        'Console.WriteLine("Ok - " & tmpToken)
                    ''        tmpToken = ""
                    ''        estado = 0
                    ''        pos -= 1
                    ''        contadorColumnas -= 1
                    ''    Else
                    ''        tmpToken = ""
                    ''        estado = -1
                    ''        pos -= 1
                    ''        contadorColumnas -= 1
                    ''    End If
                    ''End If
                    If (Char.IsDigit(car(pos))) Then
                        If (pos = car.Count - 1) Then
                            tmpToken += car(pos)
                            If (revisarToken(tmpToken)) Then
                                tmpToken = ""
                                estado = 0
                            Else
                                'func_AgregarToken(contadorFilas, (contadorColumnas - tmpToken.Length), tmpToken, "1", "VAR/CLASE")
                                'xError(tmpToken)
                                'estado = 0
                            End If
                        Else
                            tmpToken += car(pos)
                            estado = 23
                        End If
                    Else
                        If (revisarToken(tmpToken)) Then
                            tmpToken = ""
                        Else
                            'xError(tmpToken)
                            'estado = 0
                        End If
                        tmpToken = ""
                        estado = 0
                        pos -= 1
                        contadorColumnas -= 1
                    End If


                '       ██╗
                '      ███║
                '█████╗╚██║
                '╚════╝ ██║
                '       ██║
                '       ╚═╝
                Case -1
                    'Console.WriteLine("xE")
                    'Console.WriteLine("Entra a error")
                    'If (Char.IsLetter(car(pos)) Or car(pos) = "(" Or car(pos) = ")" Or car(pos) = "{" Or car(pos) = "}" Or car(pos) = "=" Or car(pos) = "," Or car(pos) = "." Or car(pos) = "#" Or car(pos) = " " Or car(pos) = vbTab Or car(pos) = vbLf) Then
                    If (car(pos) = "(" Or car(pos) = ")" Or car(pos) = "{" Or car(pos) = "}" Or car(pos) = "=" Or car(pos) = "," Or car(pos) = "." Or car(pos) = "#" Or car(pos) = " " Or car(pos) = vbTab Or car(pos) = vbLf) Then
                        'Console.WriteLine("Error1 - " & tmpToken)
                        func_AgregarErrorLexico(contadorFilas, contadorColumnas - tmpToken.Length, tmpToken)
                        estado = 0
                        pos -= 1
                        contadorColumnas -= 1
                        tmpToken = ""
                    ElseIf (pos = car.Count - 1) Then
                        tmpToken += car(pos)
                        'estado = -1
                        'Console.WriteLine("Error2 - " & tmpToken)
                        func_AgregarErrorLexico(contadorFilas, contadorColumnas - tmpToken.Length, tmpToken)
                    Else
                        tmpToken += car(pos)
                        estado = -1
                    End If

            End Select
            contadorColumnas += 1
        Next

    End Sub

    '███████╗██╗███╗   ██╗████████╗ █████╗  ██████╗████████╗██╗ ██████╗ ██████╗ 
    '██╔════╝██║████╗  ██║╚══██╔══╝██╔══██╗██╔════╝╚══██╔══╝██║██╔════╝██╔═══██╗
    '███████╗██║██╔██╗ ██║   ██║   ███████║██║        ██║   ██║██║     ██║   ██║
    '╚════██║██║██║╚██╗██║   ██║   ██╔══██║██║        ██║   ██║██║     ██║   ██║
    '███████║██║██║ ╚████║   ██║   ██║  ██║╚██████╗   ██║   ██║╚██████╗╚██████╔╝
    '╚══════╝╚═╝╚═╝  ╚═══╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═════╝
    Private Sub analisisSintactico()
        Dim estadoL = 1
        Dim tokenID = -1
        Dim cEstado = 1
        Dim Fila = 0, Columna = 0
        Dim btitulo = False, bcuento = False, bImagen = False, bTipoVoz = False, bVolumen = False, bVelocidad = False, rSelf = False, bOperacion = False, bResaltar = False, bTexto = False, bTraducir = False
        Dim traducirB = False, resaltarB = False, textoB = False, constructorB = False
        Dim token As String = ""
        For iTokens = 0 To frm_Desarrollador.ListViewTokens.Items.Count - 1
            tokenID = frm_Desarrollador.ListViewTokens.Items(iTokens).SubItems(4).Text
            Fila = frm_Desarrollador.ListViewTokens.Items(iTokens).SubItems(1).Text
            Columna = frm_Desarrollador.ListViewTokens.Items(iTokens).SubItems(2).Text
            token = frm_Desarrollador.ListViewTokens.Items(iTokens).SubItems(3).Text
            Select Case estadoL
                Case 1
                    If (tokenID = "30" Or tokenID = "31") Then
                    Else
                        If (tokenID = "1") Then
                            estadoL = 2
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'class' Inicio de Clase")
                            estadoL = 2
                        End If
                    End If
                Case 2
                    If (tokenID = "30" Or tokenID = "31") Then
                    Else
                        If (tokenID = "29") Then
                            estadoL = 3
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'Identificador de la Clase'")
                            estadoL = 3
                        End If
                    End If
                Case 3
                    If (tokenID = "30" Or tokenID = "31") Then
                    Else
                        If (tokenID = "21") Then
                            estadoL = 4
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '{' luego del indentificador clase")
                            estadoL = 4
                        End If
                    End If
                Case 4
                    If (tokenID = "30" Or tokenID = "31") Then
                    Else
                        If (tokenID = "29") Then
                            estadoL = 5
                        ElseIf (tokenID = "2") Then
                            estadoL = 9
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba VAR o RESERVADA 'def'")
                            estadoL = 5
                        End If
                    End If
                Case 5
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "23") Then
                            estadoL = 7
                        ElseIf (tokenID = "24") Then
                            estadoL = 6
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba ',' o '='")
                            estadoL = 4
                        End If
                    End If
                Case 6
                    If (tokenID = "29") Then
                        estadoL = 5
                    Else
                        func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba VAR")
                        estadoL = 7
                    End If
                Case 7
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "27") Then
                            estadoL = 8
                        ElseIf (tokenID = "26" Or tokenID = "19" Or tokenID = "20" Or tokenID = "28" Or tokenID = "24") Then
                            If (tokenID = "24") Then
                                iTokens -= 1
                                estadoL = 8
                            Else
                                estadoL = 7
                            End If
                        ElseIf (tokenID = "2") Then
                            estadoL = 9
                        ElseIf (tokenID = "29") Then
                            estadoL = 4
                            iTokens -= 1
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'string' o 'Ec' o 'Numero'")
                            estadoL = 8
                        End If
                    End If
                Case 8
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "24") Then
                            estadoL = 7
                        ElseIf (tokenID = "29") Then
                            estadoL = 5
                        ElseIf (tokenID = "2") Then
                            estadoL = 9
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba ','")
                            estadoL = 9
                        End If
                    End If
                Case 9
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "3" Or tokenID = "16" Or tokenID = "17" Or tokenID = "18") Then
                            If (tokenID = "3") Then
                                constructorB = True
                                estadoL = 10
                            ElseIf (tokenID = "16" Or tokenID = "17" Or tokenID = "18") Then
                                estadoL = 21
                                iTokens -= 1
                            End If
                            'estadoL = 10
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'constructor' o 'resaltar/mostrar/traducir'")
                        End If
                    End If
                Case 10
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "19") Then
                            estadoL = 11
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '(' luego de la palabra __init__")
                            estadoL = 11
                        End If
                    End If
                Case 11
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "13") Then
                            estadoL = 12
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'self' luego del parentesis de apertura")
                            estadoL = 12
                        End If
                    End If
                Case 12
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "20") Then
                            estadoL = 13
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba ')' luego de la palabra reservada self")
                            estadoL = 13
                        End If
                    End If
                Case 13
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "21") Then
                            estadoL = 14
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '{'")
                            estadoL = 14
                        End If
                    End If
                Case 14
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "13") Then
                            rSelf = True
                        ElseIf (rSelf) Then
                            If (tokenID = "25") Then
                                'estadoL = 15
                                rSelf = False
                            Else
                                func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '.' luego de self")
                                rSelf = False
                            End If
                        ElseIf (tokenID = "4" Or tokenID = "5" Or tokenID = "6" Or tokenID = "7" Or tokenID = "8" Or tokenID = "9") Then
                            'If (tokenID = tokenID = "4" Or tokenID = "5" Or tokenID = "6" Or tokenID = "7") Then
                            estadoL = 15
                            iTokens -= 1
                            'Else
                            '    estadoL = 16
                            'End If
                        ElseIf (tokenID = "22") Then
                            estadoL = 20
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'self' o un atributo o '}'")
                        End If
                    End If
                    'If (tokenID = "30" Or tokenID = "31") Then

                    'Else
                    '    If (tokenID = "13" Or rPalabras) Then
                    '        estadoL = 15
                    '    ElseIf (tokenID = "4" Or tokenID = "5" Or tokenID = "6" Or tokenID = "7" Or tokenID = "8" Or tokenID = "9") Then
                    '        'If (tokenID = "4" Or tokenID = "5" Or tokenID = "6" Or tokenID = "7") Then
                    '        '    rPalabras = True

                    '        'ElseIf (tokenID = "8" Or tokenID = "9") Then
                    '        '    estadoL = 16
                    '        'End If
                    '        estadoL = 15

                    '    Else
                    '        func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'self' o un atributo")
                    '        estadoL = 15
                    '    End If
                    'End If
                Case 15
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "4" And btitulo = False) Then
                            btitulo = True
                            estadoL = 16
                        ElseIf (tokenID = "5" And bcuento = False) Then
                            bcuento = True
                            estadoL = 16
                        ElseIf (tokenID = "6" And bImagen = False) Then
                            bImagen = True
                            estadoL = 16
                        ElseIf (tokenID = "7" And bTipoVoz = False) Then
                            bTipoVoz = True
                            estadoL = 16
                        ElseIf (tokenID = "8" And bVolumen = False) Then
                            bVolumen = True
                            estadoL = 17
                        ElseIf (tokenID = "9" And bVelocidad = False) Then
                            bVelocidad = True
                            estadoL = 17
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "No se puede volver utlizar 2 veces la palabra reservada")
                            'estadoL = 16
                        End If
                    End If
                Case 16
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "23") Then
                            estadoL = 18
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '='")
                            estadoL = 18
                        End If
                    End If
                Case 17
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "23") Then
                            estadoL = 19
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '='")
                            estadoL = 19
                        End If
                    End If
                Case 18
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "29" Or tokenID = "27") Then
                            estadoL = 14
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'String' o 'VAR'")
                            estadoL = 14
                        End If
                    End If
                Case 19
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "29" Or tokenID = "26" Or tokenID = "28" Or tokenID = "19" Or tokenID = "20") Then
                            If (tokenID = "29") Then
                                estadoL = 14
                            Else
                                bOperacion = True
                            End If
                        ElseIf (bOperacion And tokenID = "13" Or bOperacion And tokenID = "4" Or bOperacion And tokenID = "5" Or bOperacion And tokenID = "6" Or bOperacion And tokenID = "7" Or bOperacion And tokenID = "8" Or bOperacion And tokenID = "9") Then
                            iTokens -= 1
                            estadoL = 14
                        ElseIf (tokenID = "22") Then
                            estadoL = 20
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'Operador' o 'Valor Numerico'")
                            estadoL = 14
                        End If
                    End If
                Case 20

                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "2") Then
                            estadoL = 21
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba reservada def")
                        End If
                    End If
                Case 21

                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "16" Or tokenID = "17" Or tokenID = "18" Or tokenID = "3") Then
                            If (tokenID = "16") Then
                                resaltarB = True
                                bResaltar = True
                                estadoL = 22
                            ElseIf (tokenID = "17") Then
                                textoB = True
                                bTexto = True
                                estadoL = 22
                            ElseIf (tokenID = "18") Then
                                traducirB = True
                                bTraducir = True
                                estadoL = 22
                            ElseIf (tokenID = "3") Then
                                iTokens -= 1
                                estadoL = 9
                            End If

                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba reservada 'resaltar_palabra' o 'mostrar_texto_cuento' o 'traducir_a_voz' o 'constructor'")
                        End If
                    End If
                Case 22
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "19") Then
                            estadoL = 23
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '('")
                            estadoL = 23
                        End If
                    End If
                Case 23
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "13") Then
                            estadoL = 24
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba reservada 'self'")
                            estadoL = 24
                        End If
                    End If
                Case 24
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "20") Then
                            estadoL = 25
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba ')'")
                            estadoL = 25
                        End If
                    End If
                Case 25
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "21") Then
                            estadoL = 26
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '{'")
                            estadoL = 26
                        End If
                    End If
                Case 26
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "13") Then
                            estadoL = 27
                        ElseIf (tokenID = "11" Or tokenID = "12" Or tokenID = "10") Then
                            If (bResaltar And tokenID = "11") Then
                                bResaltar = False

                            ElseIf (bTexto And tokenID = "12") Then
                                bTexto = False

                            ElseIf (bTraducir And tokenID = "10") Then
                                bTraducir = False

                            Else
                                func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba reservada 'resaltar/texto/traduccion según su metodo'")
                            End If
                            estadoL = 28
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'self' o 'resaltar' o 'texto' o 'traduccion'")
                        End If
                    End If
                Case 27
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "25") Then
                            estadoL = 26
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '.'")
                            estadoL = 26
                        End If
                    End If
                Case 28
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "23") Then
                            estadoL = 30
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '='")
                        End If
                    End If
                Case 29
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "11" Or tokenID = "12" Or tokenID = "10") Then
                            If (bResaltar And tokenID = "11") Then
                                bResaltar = False

                            ElseIf (bTexto And tokenID = "12") Then
                                bTexto = False

                            ElseIf (bTraducir And tokenID = "10") Then
                                bTraducir = False
                            Else
                                func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba reservada 'resaltar/texto/traduccion según su metodo'")
                            End If
                            estado = 28
                        End If
                    End If
                Case 30
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "29" Or tokenID = "14" Or tokenID = "15") Then
                            estadoL = 31
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'VAR' o 'True/False'")
                            estadoL = 31
                        End If
                    End If
                Case 31
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "22") Then
                            estadoL = 32
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '}'")
                            estadoL = 32
                        End If
                    End If
                Case 32
                    If (tokenID = "30" Or tokenID = "31") Then

                    Else
                        If (tokenID = "2") Then
                            estadoL = 21
                        ElseIf (tokenID = "22") Then
                            If (constructorB = False) Then
                                func_AgregarErrorSintactico("x", "x", token, "Falta constructor")
                            End If
                            If (resaltarB = False) Then
                                func_AgregarErrorSintactico("x", "x", token, "Falta metodo obligatorio 'resaltar_palabra'")
                            End If
                            If (textoB = False) Then
                                func_AgregarErrorSintactico("x", "x", token, "Falta metodo obligatorio 'mostrar_texto_cuento'")
                            End If
                            If (traducirB = False) Then
                                func_AgregarErrorSintactico("x", "x", token, "Falta metodo obligatorio 'traducir_a_voz'")
                            End If
                            constructorB = False
                            resaltarB = False
                            textoB = False
                            traducirB = False
                            btitulo = False
                            bcuento = False
                            bImagen = False
                            bTipoVoz = False
                            bVolumen = False
                            bVelocidad = False
                            rSelf = False
                            bOperacion = False
                            bResaltar = False
                            bTexto = False
                            bTraducir = False
                            estadoL = 1
                        Else
                            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'def' o '}'")
                            constructorB = False
                            resaltarB = False
                            textoB = False
                            traducirB = False
                            btitulo = False
                            bcuento = False
                            bImagen = False
                            bTipoVoz = False
                            bVolumen = False
                            bVelocidad = False
                            rSelf = False
                            bOperacion = False
                            bResaltar = False
                            bTexto = False
                            bTraducir = False
                            estadoL = 1
                        End If
                    End If
                    'Case 15
                    '    If (tokenID = "30" Or tokenID = "31") Then

                    '    Else
                    '        If (tokenID = "25") Then
                    '            estadoL = 16
                    '        Else
                    '            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '.'")
                    '            estadoL = 17
                    '        End If
                    '    End If
                    'Case 17
                    '    If (tokenID = "30" Or tokenID = "31") Then

                    '    Else
                    '        If (tokenID = "23") Then
                    '            estadoL = 18
                    '        Else
                    '            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '='")
                    '            estadoL = 18
                    '        End If
                    '    End If
                    'Case 18
                    '    If (tokenID = "30" Or tokenID = "31") Then

                    '    Else
                    '        If (tokenID = "27") Then
                    '            estadoL = 19
                    '        ElseIf (tokenID = "28" Or tokenID = "") Then
                    '        Else
                    '            func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '='")
                    '            estadoL = 20
                    '        End If
                    '    End If
            End Select
            'Select Case estadoL
            '    Case 1
            '        'Console.WriteLine("---->" & tokenID)
            '        If (tokenID = "30" Or tokenID = "31") Then

            '        Else
            '            Select Case cEstado
            '                Case 1
            '                    If (tokenID = "1") Then
            '                        cEstado = 2
            '                    Else
            '                        func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'class' Inicio de Clase")
            '                        cEstado = 2
            '                    End If
            '                Case 2
            '                    If (tokenID = "29") Then
            '                        cEstado = 3
            '                    Else
            '                        func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'Nombre de la Clase'")
            '                        cEstado = 3
            '                    End If
            '                Case 3
            '                    If (tokenID = "21") Then
            '                        estadoL = 2
            '                        cEstado = 1
            '                    Else
            '                        func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba '{' después del identificador")
            '                        estadoL = 2
            '                        cEstado = 1
            '                    End If
            '            End Select
            '        End If
            '    Case 2
            '        If (tokenID = "30" Or tokenID = "31") Then
            '        Else
            '            If (tokenID = "2") Then
            '                estadoL = 10
            '            ElseIf (tokenID = "29") Then
            '                estadoL = 3
            '            End If
            '        End If
            '    Case 3
            '        If (tokenID = "30" Or tokenID = "31") Then
            '        Else
            '            Select Case cEstado
            '                Case 1
            '                    If (tokenID = "24") Then ' ","
            '                        cEstado = 2
            '                    ElseIf (tokenID = "23") Then
            '                        cEstado = 4
            '                    Else
            '                        func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba ',' o '=' ")
            '                    End If
            '                Case 2
            '                    If (tokenID = "29") Then

            '                    ElseIf (tokenID = "24") Then
            '                    End If
            '            End Select
            '        End If
            '    Case 4

            '        If (tokenID = "30" Or tokenID = "31") Then

            '        ElseIf (tokenID = "2") Then
            '            estadoL = 10
            '        Else
            '            Select Case cEstado
            '                Case 1
            '                    If (tokenID = "29") Then
            '                        cEstado = 2
            '                    Else
            '                        func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba 'VAR' ")
            '                        cEstado = 2
            '                    End If
            '                Case 2
            '                    If (tokenID = "24") Then
            '                        cEstado = 1

            '                    ElseIf (tokenID = "23") Then
            '                        cEstado = 3
            '                    Else
            '                        func_AgregarErrorSintactico(Fila, Columna, token, "Se esperaba ',' o '=' ")
            '                        cEstado = 1
            '                    End If
            '                Case 3

            '            End Select
            '        End If
            '    Case 10
            '        If (tokenID = "30" Or tokenID = "31") Then

            '        Else
            '            Select Case cEstado
            '                Case 1
            '                    'Console.WriteLine("ola ke ase?")
            '            End Select
            '        End If
            'End Select
        Next
    End Sub
    Private Function revisarToken(tmpToken As String)
        Select Case tmpToken.ToLower
            Case "class"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "1", "Reservada Clase")
                Return True

            Case "def"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "2", "Reservada def")
                Return True

            Case "__init__"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "3", "Reservada __init__")
                Return True

            Case "titulo"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "4", "Variable")
                Return True

            Case "cuento"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "5", "Variable")
                Return True

            Case "imagen"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "6", "Variable")
                Return True

            Case "tipovoz"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "7", "Variable")
                Return True

            Case "volumen"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "8", "Variable")
                Return True

            Case "velocidad"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "9", "Variable")
                Return True

            Case "traduccion"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "10", "Variable")
                Return True

            Case "resaltar"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "11", "Variable")
                Return True

            Case "texto"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "12", "Variable")
                Return True

            Case "self"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "13", "Reservada Self")
                Return True

            Case "true"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "14", "Booleana Verdadera")
                Return True

            Case "false"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "15", "Booleana Falsa")
                Return True

            Case "resaltar_palabra"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "16", "Metodo Resaltar")
                Return True

            Case "mostrar_texto_cuento"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "17", "Metodo MostrarTexto")
                Return True

            Case "traducir_a_voz"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "18", "Metodo TraducirVoz")
                Return True

            Case "("
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "19", "Simbolo Parentesis Abierto")
                Return True

            Case ")"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "20", "Simbolo Parentesis Cerrado")
                Return True

            Case "{"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "21", "Simbolo Llave Abierto")
                Return True

            Case "}"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "22", "Simbolo Llave Cerrada")
                Return True

            Case "="
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "23", "Simbolo Igual")
                Return True

            Case ","
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "24", "Simbolo Coma")
                Return True
            Case "."
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "25", "Simbolo Punto")
                Return True

            Case "-", "+", "*", "/"
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "26", "Operador")
                Return True

            Case System.Text.RegularExpressions.Regex.Match(tmpToken.ToLower, "\""[^\""]*\""").Value
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "27", "String")
                Return True

            Case System.Text.RegularExpressions.Regex.Match(tmpToken.ToLower, "\d+").Value
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "28", "Numero Entero")
                Return True

            '[A-Za-z][0-9]*[A-Za-z]*[0-9]*(_[0-9]*[A-Za-z]*[0-9]*[A-Za-z]*)*
            '[A-Za-z]+[0-9]*(_[A-Za-z]+[0-9]*)*
            Case System.Text.RegularExpressions.Regex.Match(tmpToken.ToLower, "([A-Za-z][0-9]*[A-Za-z]*[0-9]*)+(_[0-9]*[A-Za-z]*[0-9]*[A-Za-z]*)*").Value
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "29", "VAR/CLASE")
                Return True

            Case System.Text.RegularExpressions.Regex.Match(tmpToken.ToLower, "#.*").Value
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "30", "Comentario Simple")
                Return True

            Case System.Text.RegularExpressions.Regex.Match(tmpToken.ToLower, """""""[^\""]+""""""").Value
                func_AgregarToken(contadorFilas, (contadorColumnas - (tmpToken.Length - 1)), tmpToken, "31", "Comentario MultiLinea")
                Return True

            Case Else
                Return False
        End Select
    End Function

    Private Sub busquedaVariables()
        'Patron a buscar ([A-Za-z]+|\s*\,\s*[A-Za-z]+)+\s*=\s*
        'Patron reconociendo variables y cadenas ([A-Za-z]+|\s*\,\s*[A-Za-z]+)+\s*=\s*(\"[^\"]*\"|\s*\,\s*\"[^\"]*\")+
        'Test busqueda de numeros
        Dim coincidencias As System.Text.RegularExpressions.Match = System.Text.RegularExpressions.Regex.Match("1 blabla 2 mas blabla 3 jajaja 4", "\d")

        'Buscamos todas las coincidencias que pueden haber según el patron
        Do While coincidencias.Success
            'Console.WriteLine(coincidencias.Value)
            coincidencias = coincidencias.NextMatch()
        Loop
    End Sub

    Public Sub func_AgregarToken(Fila As String, Columna As String, Lexema As String, IDToken As String, Token As String)
        'Console.WriteLine(Fila & "_" & Columna & "_" & Lexema & "_" & IDToken & "_" & Token)
        'Console.WriteLine("columna -> " & Columna & "Lexema -> " & Lexema.Length)
        Dim frmListview As ListView
        If FormObjs.Controls.ContainsKey("ListViewTokens") Then
            frmListview = FormObjs.Controls("ListViewTokens")
            Dim tablaLexemas As New ListViewItem
            frmListview.BeginUpdate()
            tablaLexemas = frmListview.Items.Add(contadorT)
            tablaLexemas.SubItems.Add(Fila)
            tablaLexemas.SubItems.Add(Columna)
            tablaLexemas.SubItems.Add(Lexema)
            tablaLexemas.SubItems.Add(IDToken)
            tablaLexemas.SubItems.Add(Token)
            'frm_Principal.frmds.frm_Desarrollador.contadorLex += 1
            frmListview.Update()
            frmListview.EndUpdate()
        End If
        contadorT += 1
    End Sub

    Public Sub func_AgregarErrorLexico(Fila As String, Columna As String, Lexema As String)
        Dim frmListview As ListView
        If FormObjs.Controls.ContainsKey("ListViewErroresLexicos") Then
            frmListview = FormObjs.Controls("ListViewErroresLexicos")
            Dim tablaLexemas As New ListViewItem
            frmListview.BeginUpdate()
            tablaLexemas = frmListview.Items.Add(contadorErroresL)
            tablaLexemas.SubItems.Add(Fila)
            tablaLexemas.SubItems.Add(Columna)
            tablaLexemas.SubItems.Add(Lexema)
            tablaLexemas.SubItems.Add("No Reconocido")
            'frm_Principal.frmds.frm_Desarrollador.contadorLex += 1
            frmListview.Update()
            frmListview.EndUpdate()
        End If
        contadorErroresL += 1
    End Sub

    Public Sub func_AgregarErrorSintactico(Fila As String, Columna As String, Lexema As String, Descripcion As String)
        Dim frmListview As ListView
        If FormObjs.Controls.ContainsKey("ListViewErroresSintacticos") Then
            frmListview = FormObjs.Controls("ListViewErroresSintacticos")
            Dim tablaLexemas As New ListViewItem
            frmListview.BeginUpdate()
            tablaLexemas = frmListview.Items.Add(contadorErroresS)
            tablaLexemas.SubItems.Add(Fila)
            tablaLexemas.SubItems.Add(Columna)
            tablaLexemas.SubItems.Add(Descripcion)
            'frm_Principal.frmds.frm_Desarrollador.contadorLex += 1
            frmListview.Update()
            frmListview.EndUpdate()
        End If
        contadorErroresS += 1
    End Sub

    Public Sub func_limpiarToken()
        Dim frmListview As ListView
        If FormObjs.Controls.ContainsKey("ListViewTokens") Then
            frmListview = FormObjs.Controls("ListViewTokens")
            frmListview.Items.Clear()
        End If
        Dim frmListview2 As ListView
        If FormObjs.Controls.ContainsKey("ListViewErroresLexicos") Then
            frmListview2 = FormObjs.Controls("ListViewErroresLexicos")
            frmListview2.Items.Clear()
        End If
        Dim frmListview3 As ListView
        If FormObjs.Controls.ContainsKey("ListViewErroresSintacticos") Then
            frmListview3 = FormObjs.Controls("ListViewErroresSintacticos")
            frmListview3.Items.Clear()
        End If
    End Sub

    Public Sub AnalisisDatos(Texto)
        Dim datos As New InterpretacionDatos(Texto)
    End Sub
End Class
