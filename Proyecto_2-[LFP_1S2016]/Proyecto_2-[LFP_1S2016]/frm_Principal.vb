﻿'Este proyecto fue creado por Mike Molina, estudiante de la Universidad de San Carlos de Guatemala.
'Repositorio Git https://bitbucket.org/LEO318x/lenguajes-formales-y-programaci-n-proyecto-2
'Visita mi página Web: http://leo318x.ml

Public Class frm_Principal
    Private Sub frm_Principal_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing


        If e.CloseReason = System.Windows.Forms.CloseReason.UserClosing Then
            'Cerrado desde la x del formulario o Alt + F4
            Dim salirOpcion As Integer = MessageBox.Show("¿Estas seguro que deseas salir?", "Salir", MessageBoxButtons.YesNo)

            If (salirOpcion = DialogResult.Yes) Then
                'Se provoca el fin del mundo!!!! OMG!!! XD
                e.Cancel = False
            Else
                'El mundo es salvado xD
                e.Cancel = True
            End If
        Else
            'Cerrado por otra razón      

        End If
    End Sub

    Private Sub btn_Desarrollador_Click(sender As Object, e As EventArgs) Handles btn_Desarrollador.Click
        abrirVentanaDesarrollador()
    End Sub

    Private Sub btn_Cuentos_Click(sender As Object, e As EventArgs) Handles btn_Cuentos.Click
        abrirVentanaMaestroPadre()
    End Sub

    Private Sub btn_SalirAplicacion_Click(sender As Object, e As EventArgs) Handles btn_SalirAplicacion.Click
        salirAplicacion()
    End Sub

    Private Sub abrirVentanaDesarrollador()
        Me.Hide()

        '    Dim frmds As New frm_Desarrollador()
        '    frmds.Show()

        frm_Desarrollador.Show()
    End Sub

    Private Sub abrirVentanaMaestroPadre()
        Me.Hide()
        frm_Cuentos.Show()
    End Sub

    Private Sub salirAplicacion()
        Dim salirOpcion As Integer = MessageBox.Show("¿Estas seguro que deseas salir?", "Salir", MessageBoxButtons.YesNo)

        If (salirOpcion = DialogResult.Yes) Then
            'Fin del mundo OMG!!!!!!!!!!!!!!
            Application.Exit()
        End If
    End Sub

End Class
